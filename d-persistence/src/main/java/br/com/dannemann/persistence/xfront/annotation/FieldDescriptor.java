package br.com.dannemann.persistence.xfront.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldDescriptor {

	public int index();
	public int tabNavigatorIndex() default 0;
	public String defaultValue() default "";
	public String acceptableValues() default "";
	public String fieldNameFormatted() default "";
	public String postLabel() default "";
	public String dataGridColumn() default ""; // TODO: TO REMOVE?
	public String dataGridColumnWidth() default "";
	public String solDescriptonField() default "";
	public String solAutoCompleteMode() default "lazy";
	public String solFieldsToSelectCSV() default ""; // TODO: TO REMOVE?
	public String solDataGridColumnsCSV() default "";
	public String solDataGridColumnsToRemoveCSV() default "";
	public String solDataGridColumnsManyToOneCSV() default "";
	public String solLikeDefaults() default "";
	public String searchFieldFor() default "";
	public String fkHolderFor() default "";
	public String hierarchicalCodeIdField() default "id";
	public String hierarchicalCodeParentField() default "";
	public String compositeIDFields() default ""; // TODO: TO REMOVE?
	public String customADGLabelFunction() default "";
	public String mask() default "";
	public String imageField() default "";
	public String groupByThisField() default "";
	public String manyToManyTarget() default "";
	
	
	
	public boolean requiredForSearch() default false;
	public boolean bypassRequiredFieldsForSearch() default false;
	public byte minLengthForSearch() default 0;
	
	
	
	public boolean isCodeField() default false;
	public boolean isDescriptionField() default false;
	public boolean isReadonly() default false; // TODO: TO REMOVE?
	public boolean isInsertable() default true;
	public boolean isUpdatable() default true;
	public boolean isDateField() default false;
	public boolean isHourField() default false;
	public boolean isTimestamp() default false; // TODO: Verificar como vai ficar depois que eu mudar as datas para n�meros inteiros.
	public boolean isCEP() default false;
	public boolean isCNPJ() default false;
	public boolean isCPF() default false;
	public boolean isGender() default false;
	public boolean isPasswordField() default false;
	public boolean isPasswordEncrypted() default false;
	public boolean isPhoneNumber() default false;
	public boolean isRemarksField() default false;
	public boolean validateCEPNumber() default true;
	public boolean validateCNPJNumber() default true;
	public boolean validateCPFNumber() default true;
	public boolean validatePhoneNumber() default true;

}
