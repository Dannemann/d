package br.com.dannemann.persistence.domain.materialized_path;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dannemann.core.util.DUtilClass;
import br.com.dannemann.core.util.UtilList;
import br.com.dannemann.core.util.UtilObject;
import br.com.dannemann.core.util.UtilReflect;
import br.com.dannemann.core.util.UtilString;
import br.com.dannemann.persistence.DParams;
import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.domain.issue.DErrors;
import br.com.dannemann.persistence.domain.issue.DIssues;
import br.com.dannemann.persistence.domain.materialized_path.exception.ChildOfItselfException;
import br.com.dannemann.persistence.domain.materialized_path.exception.MaterializedPathException;
import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.util.UtilEntity;

public class MaterializedPathBean {

	// --------------------------------------------------------------------------
	// Fields:

	private final String entityClassName; // Complete object class name.
	private final Object entityID; // The object id.
	private final Boolean brandNewEntity; // Tells if it is a brand new object being inserted.
	private final List<Method> matPathMethods; // All annotated mat. paths methods in the object.
	private final boolean hasMatPath; // Tells if the object has at least one @MaterializedPath annotated field.
	private final Map<String, String> dbMatPaths; // The current persisted mat. paths in the database.

	// --------------------------------------------------------------------------
	// Constructors:

	public MaterializedPathBean(final Request req) {
		final Object entity = req.getObject();

		entityClassName = entity.getClass().getName();
		
		// TODO: I MUST TRY TO FIND A CUSTOM MAT. PATH APPENDER FIRST... FOR NOW I WILL JUST USE THE ID.
		
		final UtilEntity upo = new UtilEntity(entity);
		if (upo.isSinglePrimaryKey()) {
			entityID = upo.getPrimarykeyValue();
		} else
			throw new MaterializedPathException("EST", "ERRO PRECISA SER SINGLE ID.. COMPOSTO E SEM ID NAO IMPLEMENTADO AINDA", this);
		
		brandNewEntity = UtilObject.isTrue(req.get(DParams.IS_BRAND_NEW_ENTITY));
		matPathMethods = UtilReflect.getMethodsByAnnotation(MaterializedPath.class, entity); // TODO: Eliminate this process using boot-time configuration but do not eliminate the currently implementation. Give the choice to the user.
		hasMatPath = UtilList.isNotEmpty(matPathMethods); // TODO: This can also be in boot-time configuration.

		if (!brandNewEntity && hasMatPath) {
			dbMatPaths = getDBMatPathsFromRequest(req);

			if (dbMatPaths == null)
				throw new MaterializedPathException("10001", DErrors.MPATH_DBMPATH_NOTFOUND, this);

			if (dbMatPaths.size() != matPathMethods.size())
				throw new MaterializedPathException("10002", DErrors.MPATH_DBMPATH_DIFFERENT_QTD, this);
		} else
			dbMatPaths = null;
	}

	// --------------------------------------------------------------------------
	// Object overrides:

	@Override
	public String toString() {
		return
			new StringBuilder(getClass().getName())
			.append(" [entityClassName=").append(entityClassName)
			.append(", entityID=").append(entityID)
			.append(", brandNewEntity=").append(brandNewEntity)
			.append(", matPathMethods=").append(matPathMethods)
			.append(", hasMatPath=").append(hasMatPath)
			.append(", dbMatPaths=").append(dbMatPaths)
			.append("]").toString();
	}

	// --------------------------------------------------------------------------
	// Business:

	public String getAndCheckIfString(final String fieldName, final Object newMatPath) {
		if (newMatPath != null) {
			if (newMatPath instanceof String) {
				final String newMatPathStr = (String) newMatPath;

				if (newMatPathStr.isEmpty())
					throw new MaterializedPathException("10003", DErrors.get(DErrors.MPATH_EMPTY_STRING, fieldName, entityClassName), this);
				else
					return newMatPathStr;
			} else
				throw new MaterializedPathException("10004", DErrors.get(DErrors.MPATH_NOT_STRING, fieldName, entityClassName), this);
		} else
			return null;
	}

	public void checkPadding(final String fieldName, final MaterializedPath matPathAnn) {
		final byte padding = matPathAnn.padding();
		final String entityIDStr = entityID.toString();

		if (padding < 0) // Mat. path feature turned off.
			// TODO: To implement.
			return;
		else if (padding == 0) // Padding can never be zero. It makes no logical sense.
			throw new MaterializedPathException("10005", DErrors.get(DErrors.MPATH_MINIMUM_PADDING, "0", fieldName, entityClassName), this);
		else if (padding < entityIDStr.length()) // Checking if the value will exceed the padding (like trying to add "100" for a padding 2 [which supports 99 at maximum]).
			throw new MaterializedPathException("10006", DErrors.get(DErrors.MPATH_PADDING_OVERLOAD, fieldName, entityClassName, String.valueOf(padding), entityIDStr), this);
	}

	public void checkIfIsChildOfItself(final String fieldName, final MaterializedPath matPathAnn, final String newMatPath, final String dbMatPath) throws ChildOfItselfException {
		if (newMatPath != null && newMatPath.startsWith(dbMatPath.concat(matPathAnn.delimiter())))
			throw new ChildOfItselfException("50001", DIssues.get(DIssues.MPATH_CHILD_ITSELF, fieldName, DUtilClass.getSimpleNameFromComplete(entityClassName)), this);
	}

	public String addPaddedID(final MaterializedPath matPathAnn, final String newMatPath) {
		String paddedID = UtilString.padLeftWithZeroes(entityID.toString(), matPathAnn.padding()); // This variable will hold the final mat. path.
		final String delimiter = matPathAnn.delimiter();
		final String delimitedPaddedID = delimiter.concat(paddedID);

		if (newMatPath != null) {
			if (delimiter.concat(newMatPath).endsWith(delimitedPaddedID))
				return newMatPath;
			else
				return newMatPath.concat(delimitedPaddedID);
		} else
			return paddedID;
	}

	// Static:

//	public static boolean isSameParent(final MaterializedPath matPathAnn, final String newMatPath, final String dbMatPath) {
//		final int lastDelimiterOccurrence = dbMatPath.lastIndexOf(matPathAnn.delimiter()); // No delimiter means that we are already on the root.
//		return lastDelimiterOccurrence != -1 && newMatPath.equals(dbMatPath.substring(0, lastDelimiterOccurrence));
//	}

	// --------------------------------------------------------------------------
	// Utilities:

	public static Map<String, String> getDBMatPathsFromRequest(final Request req) {
		final Object matPathsInDB = req.get(DParams.DB_MATPATHS);

		if (matPathsInDB != null) {
			if (matPathsInDB instanceof Map) {
				final Map<String, String> matPathsInDBMap = (Map<String, String>) matPathsInDB;
	
				if (matPathsInDBMap.isEmpty())
					throw new MaterializedPathException("10007", DErrors.MPATH_DBMPATH_FOUNDBUTEMPTY, req);
				else
					return matPathsInDBMap;
			} else
				throw new MaterializedPathException("10008", DErrors.MPATH_DBMPATH_NOT_MAP, req);
		} else
			return null;
	}

	public static void putDBMatPathsInRequest(final Request req, final String... fieldsAndValues) {
		final Object dbMatPathsObj = req.get(DParams.DB_MATPATHS);
		final HashMap<String, String> dbMatPaths;

		if (dbMatPathsObj == null) {
			dbMatPaths = new HashMap<String, String>();
			req.put(DParams.DB_MATPATHS, dbMatPaths);
		} else if (dbMatPathsObj instanceof Map)
			dbMatPaths = (HashMap<String, String>) dbMatPathsObj;
		else
			throw new MaterializedPathException("10009", DErrors.MPATH_DBMPATH_NOT_MAP, req);

		String field = "";
		final int fieldsAndValuesLen = fieldsAndValues.length;
		for (int i = 0; i < fieldsAndValuesLen; i++)
			if (i % 2 == 0)
				field = fieldsAndValues[i]; // Field.
			else {
				dbMatPaths.put(field, fieldsAndValues[i]); // Value.
				field = "";
			}
	}

	// --------------------------------------------------------------------------
	// Getters and setters:

	public String getEntityClassName() {
		return entityClassName;
	}

	public Object getEntityID() {
		return entityID;
	}

	public Boolean isBrandNewEntity() {
		return brandNewEntity;
	}

	public List<Method> getMatPathMethods() {
		return matPathMethods;
	}

	public boolean hasMatPath() {
		return hasMatPath;
	}

	public Map<String, String> getDBMatPaths() {
		return dbMatPaths;
	}

	// --------------------------------------------------------------------------
}
