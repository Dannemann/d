package br.com.dannemann.persistence.exception;

public class MalformedTenantIDException extends DPersistenceException {

	private static final long serialVersionUID = 1L;

	public static final String MALFORMED_TENANT_ID_MSG = "Invalid tenant ID."; // TODO: I18N.
	
	public MalformedTenantIDException() {
		super(MALFORMED_TENANT_ID_MSG);
	}
	
	public MalformedTenantIDException(final String errorCode) {
		super(errorCode, MALFORMED_TENANT_ID_MSG);
	}

}
