package br.com.dannemann.persistence.test;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Random;

import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.util.UtilTenant;

// TODO: PUT THIS IN A D-TEST PROJECT.
public class EntityFactory {

	public static final ArrayList<String> WORDS = new ArrayList<String>();
	public static final Random RANDOM = new Random();

	public static final int RANDOM_STRING_LENGTH = 255;
	public static final int SEED_FOR_QUANTITY_OF_RANDOM_WORDS = 20;
	public static final int SEED_FOR_RANDOM_INTS = 1000;
	public static final int SEED_FOR_RANDOM_LONGS = 1000000;

	static {
		WORDS.add("horse");
		WORDS.add("monkey");
		WORDS.add("banana");
		WORDS.add("soap");
		WORDS.add("call");
		WORDS.add("thumb");
		WORDS.add("foot");
		WORDS.add("hair");
		WORDS.add("finger");
		WORDS.add("master");
		WORDS.add("dragon");
		WORDS.add("disk");
		WORDS.add("dry");
		WORDS.add("child");
		WORDS.add("kid");
		WORDS.add("dog");
		WORDS.add("cellphone");
		WORDS.add("belly");
		WORDS.add("cat");
		WORDS.add("super");
		WORDS.add("basic");
		WORDS.add("test");
		WORDS.add("mouse");
		WORDS.add("mice");
		WORDS.add("thunder");
		WORDS.add("avatar");
		WORDS.add("ball");
		WORDS.add("militia");
		WORDS.add("duty");
		WORDS.add("big");
	}

	public static <T extends Serializable> T entityToPersit(final Class<T> clazz) {
		T object = null;

		try {
			object = clazz.newInstance();

			for (final Method method : clazz.getMethods()) {
				final String methodName = method.getName();

				if (isValidMethodName(methodName)) {
					String getterMethodName = "g" + methodName.substring(1); // "setX" to "getX".
					Method getterMethod;
					
					try {
						getterMethod = clazz.getMethod(getterMethodName); // Getter method.
					} catch (final NoSuchMethodException e) {
						getterMethodName = "is" + methodName.substring(3); // "getX" to "isX".
						getterMethod = clazz.getMethod(getterMethodName); // Getter "is" method.
					}

					Integer parameterLength = 30;
//					Integer parameterLength = null;
//
//					if (getterMethod != null) {
//						final Column columnAnnotation = getterMethod.getAnnotation(Column.class);
//
//						if (columnAnnotation != null)
//							parameterLength = columnAnnotation.length();
//					}

					// Setter's parameter type. Getter's return type.
					final Class<?> methodParamType = method.getParameterTypes()[0];

					// The random generated argument.
					final Object randomArg = createRandomArgument(methodParamType, parameterLength);

					// Invoking the setter with the random argument.
					method.invoke(object, randomArg);
				}
			}
		} catch (final InstantiationException e) {
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			e.printStackTrace();
		} catch (final SecurityException e) {
			e.printStackTrace();
		} catch (final NoSuchMethodException e) {
			e.printStackTrace();
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return object;
	}

	public static <T extends Serializable> Request reqRespToPersist(
		final String entityManager, final Class<T> clazz, final boolean flushEntityManager)
	{
		final Request request = new Request();
		request.setTenant(entityManager);
		request.setObject(entityToPersit(clazz));
//		request.setFlushEntityManager(flushEntityManager);
		return request;
	}

	public static <T extends Serializable> Request mockPersist(final Class<T> clazz) {
		return reqRespToPersist(UtilTenant.globalDevTenant, clazz, true); // TODO: CHANGE THIS.
	}

	public static <T extends Serializable> Request mockPersist(final String entityManager, final Class<T> clazz) {
		return reqRespToPersist(entityManager, clazz, true);
	}

	protected static Object createRandomArgument(final Class<?> clazz, final Integer length) {
		if (clazz.equals(String.class)) {
			return createRandomString(length);
		} else if (clazz.equals(Integer.class)) {
			return defaultRandomIntForIntParams();
		}

		final String clazzName = clazz.getCanonicalName();

		if (clazzName.equals("int")) {
			return defaultRandomIntForIntParams();
		} else if (clazzName.equals("long")) {
			return defaultRandomIntForLongParams();
		}

		return null;
	}

	protected static String createRandomString(Integer length) {
		int randomNumOfWords = 0;
		do {
			randomNumOfWords = RANDOM.nextInt(SEED_FOR_QUANTITY_OF_RANDOM_WORDS);
		} while (randomNumOfWords == 0);

		final StringBuilder sb = new StringBuilder();

		for (int i = 0; i < randomNumOfWords; i++)
			sb.append(WORDS.get(RANDOM.nextInt(WORDS.size()))).append(" ");

		if (length == null) {
			length = RANDOM_STRING_LENGTH;
		}

		String randomString = sb.toString().trim();

		if (randomString.length() > length) {
			randomString = randomString.substring(0, length);
		}

		return randomString;
	}

	protected static boolean isValidMethodName(final String methodName) {
		return !(methodName.equals("setId") || methodName.equals("setVersion") || methodName.equals("setUuid") || methodName.startsWith("setMatPath")) 
				&&
				methodName.startsWith("set");
	}

	protected static int defaultRandomIntForIntParams() {
		return RANDOM.nextInt(SEED_FOR_RANDOM_INTS);
	}

	protected static int defaultRandomIntForLongParams() {
		return RANDOM.nextInt(SEED_FOR_RANDOM_LONGS);
	}

}
