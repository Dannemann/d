package br.com.dannemann.persistence.domain.issue;

// TODO: Internationalize this for multitenancy. This class and it's interface will change drastically.
public final class DIssues extends AbstractIssue {

	// -------------------------------------------------------------------------
	// Fields:

	public static final String MPATH_CHILD_ITSELF = "You can not add a node in the hierarchy as a child of itself. Please choose another node (field \":arg1\" on object :arg2).";

	// -------------------------------------------------------------------------
}
