package br.com.dannemann.persistence.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>TODO: <b>Important: </b>Currently working only on annotated <b>METHODS</b>. Annotated fields will fail.</p>
 * <p>TODO: <b>Important: </b>Only numeric based mat. paths supported.</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MaterializedPath {

	String fieldName() default "";

	String delimiter() default ".";

    /**
     * <p>Enables padding and sets the padding length.</p>
     * <p><b>Example:</b> A padding 3 will generate mat. paths like "001.002", a padding 5 will generate "00001.00002" and etc.
     * @return The padding length.
     */
	byte padding() default -1;

}
