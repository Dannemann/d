package br.com.dannemann.persistence.exception.translator.jdbc;

import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.dannemann.core.SupportedLocales;
import br.com.dannemann.core.util.UtilString;
import br.com.dannemann.persistence.exception.DPersistenceException;

public final class PostgreSQLExceptionTranslator {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Static:

	// -------------------------------------------------------------------------
	// Fields:

	private static final String MESSAGE_BUNDLE_FILE_NAME = "MessagesBundlePostgreSQL";
	private static final String org_postgresql_PKG = "org.postgresql";

	private static ResourceBundle BUNDLE_en_US;
	private static ResourceBundle BUNDLE_pt_BR;

	// -------------------------------------------------------------------------
	// Translation:

	/**
	 * Translates exceptions from org.postgresql.**.* package.
	 */
	public static DPersistenceException translate(final SQLException sqlException, final String localeTag) {
		final String sqlState = sqlException.getSQLState();

		if (UtilString.isEmpty(sqlState))
			throw new DPersistenceException("TODO", "NO SQL STATE IN THE PostgreSQLExceptionTranslator.translate()");

		final String originalExceptionMessage = sqlException.getLocalizedMessage();
		final String[] fieldAndValue = extractFieldAndValueFromMessage(originalExceptionMessage);

		final DPersistenceException translationDetails = new DPersistenceException(getFromBundle(sqlState, localeTag), sqlException);
		translationDetails.setCode(sqlState);
		translationDetails.setField(fieldAndValue[0]);
		translationDetails.setValue(fieldAndValue[1]);

		return translationDetails;
	}

	// Specific translators:

	/**
	 * <p>This method is responsible to extract the problematic field and value (if available) from the PostgreSQL exception message.</p>
	 * <p><b>Important:</b> Hard-coded stuff must go here.</p> 
	 */
	private static String[] extractFieldAndValueFromMessage(final String message) {
		// SQL state 23505.
		// Matcher for: ERROR: duplicate key value violates unique constraint "uk_8cwh9pf4k6kc454o3nphlijcs"\nDetalhe: Key (field)=(value) already exists.
		final Pattern p = Pattern.compile(".+\\s\\((\\w+)\\)=\\((\\w+)\\)\\s.+", Pattern.DOTALL); // Matches " (field)=(value) ".
		final Matcher m = p.matcher(message);

		if (m.matches()) {
			final String field = m.group(1);
			final String value = m.group(2);

			return new String[] { field, value };
		} else
			return new String[2];
	}

	// -------------------------------------------------------------------------
	// Utilities

	// Public:

	public static boolean isPostgreSQLException(final Throwable t) {
		return t.getClass().getName().startsWith(org_postgresql_PKG);
	}

	// Private:

	private static String getFromBundle(final String bundleKey, final String localeTag) {
		String message = null;

		if (SupportedLocales.is_pt_BR(localeTag))
			message = getBundle_pt_BR().getString(bundleKey);
		else
			message = getBundle_en_US().getString(bundleKey);

		if (UtilString.isEmpty(message))
			throw new DPersistenceException("TODO", "COULD NOT RETRIEVE MESSAGE FROM HIBERNATE BUNDLE.. EXCEPTION TRANSLATOR");

		return message;
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	private static ResourceBundle getBundle_en_US() {
		if (BUNDLE_en_US == null)
			BUNDLE_en_US = ResourceBundle.getBundle(MESSAGE_BUNDLE_FILE_NAME, SupportedLocales.LOCALE_en_US);

		return BUNDLE_en_US;
	}

	private static ResourceBundle getBundle_pt_BR() {
		if (BUNDLE_pt_BR == null)
			BUNDLE_pt_BR = ResourceBundle.getBundle(MESSAGE_BUNDLE_FILE_NAME, SupportedLocales.LOCALE_pt_BR);

		return BUNDLE_pt_BR;
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Instance:

	// Constructors:

	private PostgreSQLExceptionTranslator() {
	}

	// -------------------------------------------------------------------------
}
