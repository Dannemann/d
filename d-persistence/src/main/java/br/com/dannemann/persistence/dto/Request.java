package br.com.dannemann.persistence.dto;

/**
 * This class is a map. You can extend it to create your own request and add your specific request getters and setters.
 */	
public class Request extends br.com.dannemann.core.pojo.Request {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// Generic persistence request properties:

	public static final String id = "id";
	public static final String query = "query";

	// -------------------------------------------------------------------------
	// Getters and setters:

	public Object getId() {
		return get(id);
	}

	public void setId(final Object id) {
		put(Request.id, id);
	}

	public Object getQuery() {
		return get(query);
	}

	public void setQuery(final Object query) {
		put(Request.query, query);
	}

	// -------------------------------------------------------------------------
	// Builders:

	public static Request build() {
		return new Request();
	}

	public Request withTenant(final String tenant) {
		setTenant(tenant);
		return this;
	}

	public Request withObject(final Object object) {
		setObject(object);
		return this;
	}

	public Request withObjectType(final String objectType) {
		setObjectType(objectType);
		return this;
	}

	public Request withLocaleTag(final String localeTag) {
		setLocaleTag(localeTag);
		return this;
	}

	public Request withKeyValue(final String key, final Object value) {
		put(key, value);
		return this;
	}

	// -------------------------------------------------------------------------
}
