package br.com.dannemann.persistence.domain.xfront.dto;

public class CrudDescriptorDto {

	//--------------------------------------------------------------------------
	// Fields:

	private String classNameFormatted;
	private String menuNode;
	private String menuIcon;
	private boolean useButtonSearch;
	private boolean useButtonSearchAll;
	private boolean useButtonInsertNewRow;
	private boolean useButtonSaveOrUpdate;
	private boolean useButtonExclude;
	private int numberOfGridColumnsOnForm;
	private String tabs;
	private String usingAddressForm;
	private String fileUploaders;
	private String dataGridColumns;
	private String executeAfterSave;
	private boolean searchAllOnOpen;
	private boolean cachedCRUDForm;
	private String customCRUDForm;
	private boolean manyToManyAssocTable;

	// Auto filled.

	private String tableName;
	private String className;
	private String classSimpleName;

	//--------------------------------------------------------------------------
	// Getters and setters:

	// CRUD descriptor indicator:

	public boolean getIsCRUDDescriptor() {
		return true;
	}

	public void setIsCRUDDescriptor(boolean isCRUDDescriptor) {
	}

	// Getters and setters:

	public String getClassNameFormatted() {
		return classNameFormatted;
	}

	public void setClassNameFormatted(String classNameFormatted) {
		this.classNameFormatted = classNameFormatted;
	}

	public String getMenuNode() {
		return menuNode;
	}

	public void setMenuNode(String menuNode) {
		this.menuNode = menuNode;
	}

	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}

	public boolean isUseButtonSearch() {
		return useButtonSearch;
	}

	public void setUseButtonSearch(boolean useButtonSearch) {
		this.useButtonSearch = useButtonSearch;
	}

	public boolean isUseButtonSearchAll() {
		return useButtonSearchAll;
	}

	public void setUseButtonSearchAll(boolean useButtonSearchAll) {
		this.useButtonSearchAll = useButtonSearchAll;
	}

	public boolean isUseButtonInsertNewRow() {
		return useButtonInsertNewRow;
	}

	public void setUseButtonInsertNewRow(boolean useButtonInsertNewRow) {
		this.useButtonInsertNewRow = useButtonInsertNewRow;
	}

	public boolean isUseButtonSaveOrUpdate() {
		return useButtonSaveOrUpdate;
	}

	public void setUseButtonSaveOrUpdate(boolean useButtonSaveOrUpdate) {
		this.useButtonSaveOrUpdate = useButtonSaveOrUpdate;
	}

	public boolean isUseButtonExclude() {
		return useButtonExclude;
	}

	public void setUseButtonExclude(boolean useButtonExclude) {
		this.useButtonExclude = useButtonExclude;
	}

	public int getNumberOfGridColumnsOnForm() {
		return numberOfGridColumnsOnForm;
	}

	public void setNumberOfGridColumnsOnForm(int numberOfGridColumnsOnForm) {
		this.numberOfGridColumnsOnForm = numberOfGridColumnsOnForm;
	}

	public String getTabs() {
		return tabs;
	}

	public void setTabs(String tabs) {
		this.tabs = tabs;
	}

	public String getUsingAddressForm() {
		return usingAddressForm;
	}

	public void setUsingAddressForm(String usingAddressForm) {
		this.usingAddressForm = usingAddressForm;
	}

	public String getFileUploaders() {
		return fileUploaders;
	}

	public void setFileUploaders(String fileUploaders) {
		this.fileUploaders = fileUploaders;
	}

	public String getDataGridColumns() {
		return dataGridColumns;
	}

	public void setDataGridColumns(String dataGridColumns) {
		this.dataGridColumns = dataGridColumns;
	}

	public String getExecuteAfterSave() {
		return executeAfterSave;
	}

	public void setExecuteAfterSave(String executeAfterSave) {
		this.executeAfterSave = executeAfterSave;
	}

	public boolean isCachedCRUDForm() {
		return cachedCRUDForm;
	}

	public void setCachedCRUDForm(boolean cachedCRUDForm) {
		this.cachedCRUDForm = cachedCRUDForm;
	}

	public boolean getSearchAllOnOpen() {
		return searchAllOnOpen;
	}

	public void setSearchAllOnOpen(boolean searchAllOnOpen) {
		this.searchAllOnOpen = searchAllOnOpen;
	}

	public String getCustomCRUDForm() {
		return customCRUDForm;
	}

	public void setCustomCRUDForm(String customCRUDForm) {
		this.customCRUDForm = customCRUDForm;
	}

	public boolean isManyToManyAssocTable() {
		return manyToManyAssocTable;
	}

	public void setManyToManyAssocTable(boolean manyToManyAssocTable) {
		this.manyToManyAssocTable = manyToManyAssocTable;
	}

	// Auto filled.

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassSimpleName() {
		return classSimpleName;
	}

	public void setClassSimpleName(String classSimpleName) {
		this.classSimpleName = classSimpleName;
	}

	//--------------------------------------------------------------------------
}
