package br.com.dannemann.persistence.domain.materialized_path.exception;

// TODO: MUST BE CHECKED EXCEPTION.
public class ChildOfItselfException extends MaterializedPathException {

	// --------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// --------------------------------------------------------------------------
	// Constructors:

	public ChildOfItselfException(final String errorCode, final String message, final Object entity) {
		super(errorCode, message, entity);
	}

	// --------------------------------------------------------------------------
}
