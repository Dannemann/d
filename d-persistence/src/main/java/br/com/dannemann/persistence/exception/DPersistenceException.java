package br.com.dannemann.persistence.exception;

import br.com.dannemann.core.exception.DRuntimeException;

public class DPersistenceException extends DRuntimeException {

	// --------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// --------------------------------------------------------------------------
	// Constructors:

	public DPersistenceException(final String message) {
		super(message);
	}

	public DPersistenceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DPersistenceException(final String code, final String message) {
		super(code, message);
	}

	public DPersistenceException(final String code, final String message, final Object source) {
		super(code, message, source);
	}

	// --------------------------------------------------------------------------
}
