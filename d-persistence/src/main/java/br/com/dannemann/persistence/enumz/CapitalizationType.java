package br.com.dannemann.persistence.enumz;

public enum CapitalizationType {

	NONE,
	ALL_CAPS,
	ALL_CAPS_ALPHANUMERIC,
	ALL_CAPS_ASCII

}
