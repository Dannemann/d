package br.com.dannemann.persistence.util;

import br.com.dannemann.persistence.dto.Request;

public final class UtilTenant {

	public static String globalDevTenant = "development";

	public static Request build() {
		return new Request().withTenant(globalDevTenant);
	}

	public static Request withObject(final Object obj) {
		return build().withObject(obj);
	}

	public static Request withObjectType(final Class<?> clazz) {
		return build().withObjectType(clazz.getName());
	}

}
