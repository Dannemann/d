package br.com.dannemann.persistence.domain.xfront.resolver;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

import br.com.dannemann.core.util.DUtilString;
import br.com.dannemann.persistence.domain.xfront.dto.CrudDescriptorDto;
import br.com.dannemann.persistence.domain.xfront.dto.FieldDescriptorDto;
import br.com.dannemann.persistence.xfront.annotation.CRUDDescriptor;
import br.com.dannemann.persistence.xfront.annotation.FieldDescriptor;

public class XFrontMetadataResolver {

	//--------------------------------------------------------------------------
	// Fields:

	protected static final Comparator<FieldDescriptorDto> fieldDescriptorOrderComparator = new Comparator<FieldDescriptorDto>() {
		public int compare(final FieldDescriptorDto e1, final FieldDescriptorDto e2) {
			return Integer.valueOf(e1.getIndex()).compareTo(e2.getIndex());
		}
	};

	//--------------------------------------------------------------------------
	// Public interface:

	public ArrayList<Object> resolve(final Class<?> entityClass) {
		final ArrayList<Object> entityDescriptorsToReturn = new ArrayList<Object>(); // The return object.

		final CrudDescriptorDto crudDescriptorDto = new CrudDescriptorDto();
		resolveClassAnnotations(entityClass, crudDescriptorDto);
		entityDescriptorsToReturn.add(crudDescriptorDto);

		final Method[] entityMethods = entityClass.getDeclaredMethods();
		for (final Method method : entityMethods) {
			final String methodName = method.getName();

			// TODO: LIMITATION TO ONLY ANNOTATED GETTER METHODS HERE. ALSO IMPLEMENT FOR FIELDS.
			// TODO: SHOULD I USE INTROSPECTOR HERE? IF NOT, REGEXP? https://stackoverflow.com/questions/8524011/java-reflection-how-can-i-get-the-all-getter-methods-of-a-java-class-and-invoke
			if (methodName.startsWith("get")) {
				final FieldDescriptorDto fieldDescriptorDto = new FieldDescriptorDto();
				resolvePropertyAnnotations(method, fieldDescriptorDto);
				entityDescriptorsToReturn.add(fieldDescriptorDto);
			}
		}

//		{###} TODO ORDENAR
//		Collections.sort(entityDescriptorsToReturn, fieldDescriptorOrderComparator);

		return entityDescriptorsToReturn;
	}

	// TODO: UNDERSTAND THIS METHOD AND REFACTOR IT.
	@SuppressWarnings("unchecked")
	public void afterResolve(final Map<String, ArrayList<Object>> entities) {
		final Iterator<?> it = entities.entrySet().iterator();

		while (it.hasNext()) {
			final Map.Entry<String, ArrayList<Object>> entity = (Map.Entry<String, ArrayList<Object>>) it.next();
//			final String objectType = object.getKey();
			final ArrayList<Object> entityDescriptors = entity.getValue();

			for (final Object annotationBean : entityDescriptors) {
				if (annotationBean instanceof FieldDescriptorDto) {
					final FieldDescriptorDto descriptorBean = (FieldDescriptorDto) annotationBean;

					if (descriptorBean.getIsManyToOne()) {
						if (descriptorBean.getDataGridColumn().indexOf(".") == -1) {
							final ArrayList<Object> manyToOneTargetEntity = entities.get(descriptorBean.getJavaType());

							for (final Object annotationBean2 : manyToOneTargetEntity) {
								if (annotationBean2 instanceof FieldDescriptorDto) {
									final FieldDescriptorDto descriptorBean2 = (FieldDescriptorDto) annotationBean2;

									if (descriptorBean2.getIsDescriptionField()) {
										descriptorBean.setDataGridColumn(
											(new StringBuilder()).append(descriptorBean.getDataGridColumn()).append(".").append(descriptorBean2.getJavaField()).toString());
									}
								}
							}
						}
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------------
	// Protected interface:

	protected void resolveClassAnnotations(final Class<?> entityClass, final CrudDescriptorDto crudDescriptorDtoToFill) {
		resolveCRUDDescriptorAnnotation(entityClass, crudDescriptorDtoToFill); // Resolving @CRUDDescriptor annotation.
	}

	protected void resolvePropertyAnnotations(final Method getterMethod, final FieldDescriptorDto fieldDescriptorDtoToFill) {
		// Defaults.
		fieldDescriptorDtoToFill.setDataGridColumn(DUtilString.getterSettterToFieldName(getterMethod.getName()));
		fieldDescriptorDtoToFill.setFieldNameFormatted(DUtilString.camelCaseToTextWithSpaces(getterMethod.getName())); // TODO: FLEX MUST DO THIS BUSINESS RELATED TRANSFORMATIONS.
		fieldDescriptorDtoToFill.setJavaField(DUtilString.getterSettterToFieldName(getterMethod.getName()));
		fieldDescriptorDtoToFill.setJavaType(getterMethod.getReturnType().getName());
		fieldDescriptorDtoToFill.setSolClassName(getterMethod.getReturnType().getName()); // TODO: TO REMOVE?

		resolveFieldDescriptorAnnotation(getterMethod.getAnnotation(FieldDescriptor.class), fieldDescriptorDtoToFill); // Resolving @FieldDescriptor annotation.
	}

	//--------------------------------------------------------------------------
	// Private interface:

	private void resolveCRUDDescriptorAnnotation(final Class<?> entityClass, final CrudDescriptorDto crudDescriptorDtoToFill) {
		final CRUDDescriptor crudDescriptorAnnotation = entityClass.getAnnotation(CRUDDescriptor.class);
		if (crudDescriptorAnnotation != null) {
//			final Table tableAnnotation = entityClass.getAnnotation(Table.class); // TODO: ON JPA IMPLEMENTATION.

			if (DUtilString.hasValue(crudDescriptorAnnotation.classNameFormatted()))
				crudDescriptorDtoToFill.setClassNameFormatted(crudDescriptorAnnotation.classNameFormatted());
			else
				crudDescriptorDtoToFill.setClassNameFormatted(entityClass.getSimpleName());

			crudDescriptorDtoToFill.setMenuNode(crudDescriptorAnnotation.menuNode());
			crudDescriptorDtoToFill.setMenuIcon(crudDescriptorAnnotation.menuIcon());
			crudDescriptorDtoToFill.setUseButtonSearch(crudDescriptorAnnotation.useButtonSearch());
			crudDescriptorDtoToFill.setUseButtonSearchAll(crudDescriptorAnnotation.useButtonSearchAll());
			crudDescriptorDtoToFill.setUseButtonInsertNewRow(crudDescriptorAnnotation.useButtonInsertNewRow());
			crudDescriptorDtoToFill.setUseButtonSaveOrUpdate(crudDescriptorAnnotation.useButtonSaveOrUpdate());
			crudDescriptorDtoToFill.setUseButtonExclude(crudDescriptorAnnotation.useButtonExclude());
			crudDescriptorDtoToFill.setNumberOfGridColumnsOnForm(crudDescriptorAnnotation.numberOfGridColumnsOnForm());
			crudDescriptorDtoToFill.setTabs(crudDescriptorAnnotation.tabs());
			crudDescriptorDtoToFill.setFileUploaders(crudDescriptorAnnotation.fileUploaders());
			crudDescriptorDtoToFill.setUsingAddressForm(crudDescriptorAnnotation.usingAddressForm());
			crudDescriptorDtoToFill.setDataGridColumns(crudDescriptorAnnotation.dataGridColumns());
			crudDescriptorDtoToFill.setExecuteAfterSave(crudDescriptorAnnotation.executeAfterSave());
			crudDescriptorDtoToFill.setSearchAllOnOpen(crudDescriptorAnnotation.searchAllOnOpen());
			crudDescriptorDtoToFill.setCachedCRUDForm(crudDescriptorAnnotation.cachedCRUDForm());
			crudDescriptorDtoToFill.setCustomCRUDForm(crudDescriptorAnnotation.customCRUDForm());
			crudDescriptorDtoToFill.setManyToManyAssocTable(crudDescriptorAnnotation.manyToManyAssocTable());

//			crudDescriptorBean.setTableName(tableAnnotation.name()); // TODO: ON JPA IMPLEMENTATION.
			crudDescriptorDtoToFill.setClassName(entityClass.getName());
			crudDescriptorDtoToFill.setClassSimpleName(entityClass.getSimpleName());
		}
	}

	private void resolveFieldDescriptorAnnotation(final FieldDescriptor fieldDescriptorAnnotation, final FieldDescriptorDto fd) {
		if (fieldDescriptorAnnotation != null) {
			// int.
			fd.setIndex(fieldDescriptorAnnotation.index());
			fd.setTabNavigatorIndex(fieldDescriptorAnnotation.tabNavigatorIndex());
			// String.
			fd.setDefaultValue(fieldDescriptorAnnotation.defaultValue());
			fd.setAcceptableValues(fieldDescriptorAnnotation.acceptableValues());

			if (DUtilString.hasValue(fieldDescriptorAnnotation.dataGridColumn()))
				fd.setDataGridColumn(fieldDescriptorAnnotation.dataGridColumn());

			if (DUtilString.hasValue(fieldDescriptorAnnotation.fieldNameFormatted()))
				fd.setFieldNameFormatted(fieldDescriptorAnnotation.fieldNameFormatted());

			fd.setDataGridColumnWidth(fieldDescriptorAnnotation.dataGridColumnWidth());
			fd.setPostLabel(fieldDescriptorAnnotation.postLabel());
			fd.setSolDescriptonField(fieldDescriptorAnnotation.solDescriptonField());
			fd.setSolAutoCompleteMode(fieldDescriptorAnnotation.solAutoCompleteMode());
			fd.setSolFieldsToSelectCSV(fieldDescriptorAnnotation.solFieldsToSelectCSV());
			fd.setSolDataGridColumnsCSV(fieldDescriptorAnnotation.solDataGridColumnsCSV());
			fd.setSolDataGridColumnsToRemoveCSV(fieldDescriptorAnnotation.solDataGridColumnsToRemoveCSV());
			fd.setSolDataGridColumnsManyToOneCSV(fieldDescriptorAnnotation.solDataGridColumnsManyToOneCSV());
			fd.setSolLikeDefaults(fieldDescriptorAnnotation.solLikeDefaults());
			fd.setSearchFieldFor(fieldDescriptorAnnotation.searchFieldFor());
			fd.setFkHolderFor(fieldDescriptorAnnotation.fkHolderFor());
			fd.setHierarchicalCodeIdField(fieldDescriptorAnnotation.hierarchicalCodeIdField());
			fd.setHierarchicalCodeParentField(fieldDescriptorAnnotation.hierarchicalCodeParentField());
			fd.setCompositeIDFields(fieldDescriptorAnnotation.compositeIDFields());
			fd.setCustomADGLabelFunction(fieldDescriptorAnnotation.customADGLabelFunction());
			fd.setMask(fieldDescriptorAnnotation.mask());
			fd.setImageField(fieldDescriptorAnnotation.imageField());
			fd.setMinLengthForSearch(fieldDescriptorAnnotation.minLengthForSearch());
			
			// Boolean.
			fd.setRequiredForSearch(fieldDescriptorAnnotation.requiredForSearch());
			fd.setBypassRequiredFieldsForSearch(fieldDescriptorAnnotation.bypassRequiredFieldsForSearch());
			
			
			fd.setIsInsertable(fieldDescriptorAnnotation.isInsertable());
			fd.setIsCodeField(fieldDescriptorAnnotation.isCodeField());
			fd.setIsDescriptionField(fieldDescriptorAnnotation.isDescriptionField());
			fd.setIsReadonly(fieldDescriptorAnnotation.isReadonly());
			fd.setIsGender(fieldDescriptorAnnotation.isGender());

			if (!fieldDescriptorAnnotation.isUpdatable())
				fd.setIsUpdatable(false);

			fd.setIsDateField(fieldDescriptorAnnotation.isDateField());
			fd.setGroupByThisField(fieldDescriptorAnnotation.groupByThisField());

//					if (fieldDescriptorBean.getGroupByThisField().equalsIgnoreCase("")) {
//						if (DUtilString.hasValue(
//							fieldDescriptorBean.getAcceptableValues()) ||
//							fieldDescriptorBean.getIsDateField() ||
//							fieldDescriptorBean.getIsGender() ||
//							returnTypeStr.equalsIgnoreCase("java.lang.Boolean") ||
//							(jpaColumnAnnotation != null ? jpaColumnAnnotation.length() == 1 : false))
//							fieldDescriptorBean.setGroupByThisField("true");
//						else
//							fieldDescriptorBean.setGroupByThisField("false");
//					} else if (fieldDescriptorBean.getGroupByThisField().equalsIgnoreCase("false"))
//						fieldDescriptorBean.setGroupByThisField("false");
//					else
//						fieldDescriptorBean.setGroupByThisField("true");

			fd.setIsHourField(fieldDescriptorAnnotation.isHourField());
			fd.setIsTimestamp(fieldDescriptorAnnotation.isTimestamp());
			fd.setIsCEP(fieldDescriptorAnnotation.isCEP());
			fd.setIsCNPJ(fieldDescriptorAnnotation.isCNPJ());
			fd.setIsCPF(fieldDescriptorAnnotation.isCPF());
			fd.setIsPasswordField(fieldDescriptorAnnotation.isPasswordField());
			fd.setIsPasswordEncrypted(fieldDescriptorAnnotation.isPasswordEncrypted());
			fd.setIsPhoneNumber(fieldDescriptorAnnotation.isPhoneNumber());
			fd.setIsRemarksField(fieldDescriptorAnnotation.isRemarksField());
			// DIssues.
			fd.setValidateCEPNumber(fieldDescriptorAnnotation.validateCEPNumber());
			fd.setValidateCNPJNumber(fieldDescriptorAnnotation.validateCNPJNumber());
			fd.setValidateCPFNumber(fieldDescriptorAnnotation.validateCPFNumber());
			fd.setValidatePhoneNumber(fieldDescriptorAnnotation.validatePhoneNumber());
		}
	}

	//--------------------------------------------------------------------------
}
