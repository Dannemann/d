package br.com.dannemann.persistence.p6spy;

import br.com.dannemann.core.util.UtilString;

/**
 * <p>Children of this class must implement <code>com.p6spy.engine.spy.appender.MessageFormattingStrategy</code> interface for the P6Spy formatter to work.</p>
 */
public class P6SpyFormatter {

	public String formatMessage(final int connectionId, final String now, final long elapsed, final String category, final String prepared, final String sql, final String url) {
		final StringBuilder sb = 
			new StringBuilder(" ### ").append(Long.toString(elapsed)).append("|").append(category).append("|").append(connectionId).append("|").append("\t").append(sql);

		if (UtilString.isNotEmpty(prepared))
			sb.append("\t\t").append(" ____________________ Prepared SQL: ").append(prepared);

		// TODO: Check what the new "url" parameter is.

		return sb.toString();
	}

}
