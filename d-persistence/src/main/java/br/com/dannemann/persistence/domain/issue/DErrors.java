package br.com.dannemann.persistence.domain.issue;

import br.com.dannemann.persistence.DParams;

public final class DErrors extends AbstractIssue {

	// -------------------------------------------------------------------------
	// Fields:

	public static final String REQUEST_NULL = "Request is null.";

	public static final String MPATH_COULDNT_RETRIEVE_FIELD_NAME = "Error retrieving the field name of a @MaterializedPath property.";
	public static final String MPATH_DBMPATH_NOTFOUND = DParams.DB_MATPATHS.concat(" was not found in the request parameters. It is required for D framework's Materialized Path feature to work correctly.");
	public static final String MPATH_DBMPATH_DIFFERENT_QTD = DParams.DB_MATPATHS.concat(" error: The number of database mat. paths that came in the request is different from the number of annotated mat. path fields in the object.");
	public static final String MPATH_DBMPATH_NOT_MAP = DParams.DB_MATPATHS.concat(" is not a java.util.Map<String, String>.");
	public static final String MPATH_DBMPATH_FOUNDBUTEMPTY = DParams.DB_MATPATHS.concat(" was found in the request \"params\" but it is empty. Check if it is set properly. It is required for D framework's Materialized Path feature to work correctly.");
	public static final String MPATH_NOT_STRING = "The field \":arg1\" on object :arg2 is not of type java.lang.String. All @MaterializedPath fields must be strings.";
	public static final String MPATH_EMPTY_STRING = "The @MaterializedPath field \":arg1\" on object :arg2 is empty (\"\").";
	public static final String MPATH_MINIMUM_PADDING = "Mat. paths padding must be bigger than :arg1 (field \":arg2\" on object :arg3).";
	public static final String MPATH_PADDING_OVERLOAD = "Mat. path will overload the padding for field \":arg1\" on object :arg2. The current padding \":arg3\" does not support the value \":arg4\".";

	// -------------------------------------------------------------------------
}
