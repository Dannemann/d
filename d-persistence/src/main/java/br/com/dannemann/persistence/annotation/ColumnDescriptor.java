package br.com.dannemann.persistence.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import br.com.dannemann.persistence.enumz.CapitalizationType;

// TODO: REMOVE THIS CLASS?

//@Retention(RetentionPolicy.RUNTIME) // TODO: Do I need runtime retention on this annotation?
@Target(ElementType.METHOD)
public @interface ColumnDescriptor {

	CapitalizationType capitalization() default CapitalizationType.NONE;

	String copyValueOf() default "";

}
