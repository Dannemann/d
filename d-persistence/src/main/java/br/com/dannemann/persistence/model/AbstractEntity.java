package br.com.dannemann.persistence.model;

import java.io.Serializable;

/**
 * This is just an abstraction in case we need further generic behavior.
 */
public abstract class AbstractEntity implements Serializable {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// -------------------------------------------------------------------------
	// Constructors:

	public AbstractEntity() {
	}

	// -------------------------------------------------------------------------
}
