package br.com.dannemann.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.dannemann.core.exception.Issue;

public class Response {

	// -------------------------------------------------------------------------
	// Fields:

	private Object result;
	private List<Issue> issues;

	// -------------------------------------------------------------------------
	// Constructors:

	public Response() {
	}

	// -------------------------------------------------------------------------
	// Object overrides:

	@Override
	public String toString() {
		return "Response [result=" + result + ", issues=" + issues + "]";
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	public Object getResult() {
		return result;
	}

	public void setResult(final Object result) {
		this.result = result;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(final List<Issue> issues) {
		this.issues = issues;
	}

	public void addIssue(final Issue issue) {
		if (issues == null)
			issues = new ArrayList<Issue>();

		issues.add(issue);
	}

	// -------------------------------------------------------------------------
}
