package br.com.dannemann.persistence.util;

import static br.com.dannemann.core.util.UtilReflect.invoke;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;

import br.com.dannemann.persistence.exception.DPersistenceException;

/**
 * <p><b>Important:</b> This class is not final/static because we can make the user extend it to add his own primary key finding functionality.</p>
 */
public class UtilEntity {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Fields:

	private final Object entity;
	private final ArrayList<Method> primaryKeyMethods = new ArrayList<Method>();

	private Object singlePrimaryKeyValue;
	private boolean isSinglePrimaryKeyValuePopulated;

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Object creation:

	// -------------------------------------------------------------------------
	// Constructors:

	public UtilEntity(final Object entity) {
		this.entity = entity;

		populatePrimaryKeyMethods();
	}

	// -------------------------------------------------------------------------
	// Initialization:

	protected void populatePrimaryKeyMethods() {
		for (final Method method : entity.getClass().getMethods()) {
			boolean found = false;

			// Trying @Id annotations.
			if (!method.isBridge() && !found) {
				for (final Annotation annotation : method.getAnnotations())
					if (annotation.annotationType().getSimpleName().equalsIgnoreCase("id")) {
						primaryKeyMethods.add(method);
						found = true;
						break;
					}
			}

			// Trying 
			if (!found) {
				
			}
		}
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Public:

	public boolean hasNoPrimaryKey() {
		return primaryKeyMethods.isEmpty();
	}

	public boolean isSinglePrimaryKey() {
		return primaryKeyMethods.size() == 1;
	}

	public boolean isCompositePrimaryKey() {
		return primaryKeyMethods.size() > 1;
	}

	public boolean hasPopulatedPrimaryKeys() {
		return getPrimarykeyValue() != null; // TODO: MUST VERIFY BOTH, SINGLE AND COMPOSITE PRIMARY KEYS. CHECKING JUST SINGLE FOR NOW.
	}

	// Single primary key:

	public Object getPrimarykeyValue() throws NoPrimaryKeyException {
		if (isSinglePrimaryKeyValuePopulated)
			return singlePrimaryKeyValue;
		else {
			if (hasNoPrimaryKey())
				throw new NoPrimaryKeyException("TODO", "HAS NO PRIMARY KEY");
			else if (!isSinglePrimaryKey())
				throw new DPersistenceException("TODO", "METHODO getPrimarykeyValue SO TRABALHA COM SINGLE PRIMARYKEY");
			
			singlePrimaryKeyValue = invoke(primaryKeyMethods.get(0), entity);
			isSinglePrimaryKeyValuePopulated = true;
			return singlePrimaryKeyValue;
		}
	}

	// Composite primary key:

	public Object[] getPrimaryKeysValues() throws NoPrimaryKeyException {
		// TODO: TO IMPLEMENT.
		throw new DPersistenceException("TODO", "NOT IMPLEMENTED YET.");
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Getters and setters:
	
	public Object getEntity() {
		return entity;
	}

	public ArrayList<Method> getPrimaryKeyMethods() {
		return primaryKeyMethods;
	}
	
	// -------------------------------------------------------------------------
}

class NoPrimaryKeyException extends DPersistenceException {

	private static final long serialVersionUID = 1L;

	public NoPrimaryKeyException(final String message) {
		super(message);
	}

	public NoPrimaryKeyException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public NoPrimaryKeyException(final String code, final String message) {
		super(code, message);
	}

	public NoPrimaryKeyException(final String code, final String message, final Object source) {
		super(code, message, source);
	}

}
