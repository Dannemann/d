package br.com.dannemann.persistence.service;

import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.dto.Response;

public interface CrudService<E, P> {

	Response findById(final Request req);
	Response findByExample(final Request req);
	Response findAll(final Request req);

	Response save(final Request req);

	Response delete(final Request req);

	Response execute(final Request req);

}
