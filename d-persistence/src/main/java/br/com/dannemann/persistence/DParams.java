package br.com.dannemann.persistence;

// TODO: ENUM?
public final class DParams {

	// --------------------------------------------------------------------------
	// Fields:

	public static final String ALL = "d:all";

	public static final String IS_BRAND_NEW_ENTITY = "d:isBrandNewEntity";

	public static final String DB_MATPATHS = "d:dbMatPaths";

	// --------------------------------------------------------------------------
}
