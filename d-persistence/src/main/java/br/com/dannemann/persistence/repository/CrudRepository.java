package br.com.dannemann.persistence.repository;

import java.util.List;

import br.com.dannemann.persistence.dto.Request;

public interface CrudRepository<E, P> {

	E findById(final Request req);
	List<E> findByExample(final Request req);
	List<E> findAll(final Request req);

	E save(final Request req);

	void delete(final Request req);

	Object execute(final Request req);

}
