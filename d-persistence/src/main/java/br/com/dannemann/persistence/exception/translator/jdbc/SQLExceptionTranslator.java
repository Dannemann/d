package br.com.dannemann.persistence.exception.translator.jdbc;

import java.sql.SQLException;

import br.com.dannemann.persistence.exception.DPersistenceException;

public final class SQLExceptionTranslator {

	// -------------------------------------------------------------------------
	// Static:

	// Translation:

	public static DPersistenceException translate(final Throwable t, final String localeTag) {
		final SQLException sqlException = searchForSQLExceptionCause(t);

		if (sqlException != null) {
			// PostgreSQL.
			if (PostgreSQLExceptionTranslator.isPostgreSQLException(sqlException))
				return PostgreSQLExceptionTranslator.translate(sqlException, localeTag);
		}

		return null;
	}

	// Utilities:

	public static SQLException searchForSQLExceptionCause(final Throwable t) {
		if (t == null)
			return null;
		else if (t instanceof SQLException)
			return (SQLException) t;
		else
			return searchForSQLExceptionCause(t.getCause());
	}

	// -------------------------------------------------------------------------
	// Instance:

	// Constructors:

	private SQLExceptionTranslator() {
	}

	// -------------------------------------------------------------------------
}
