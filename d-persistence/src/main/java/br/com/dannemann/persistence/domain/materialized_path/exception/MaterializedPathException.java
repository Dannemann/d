package br.com.dannemann.persistence.domain.materialized_path.exception;

import br.com.dannemann.persistence.exception.DPersistenceException;

public class MaterializedPathException extends DPersistenceException {

	// --------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// --------------------------------------------------------------------------
	// Constructors:

	public MaterializedPathException(final String errorCode, final String message, final Object source) {
		super(
			errorCode,
			new StringBuilder(message).append(" [").append(source.getClass().getSimpleName()).append("]").append(" ").append(source.toString()).toString(),
			source);
	}

	// --------------------------------------------------------------------------
}
