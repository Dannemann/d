package br.com.dannemann.persistence;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.dto.Request;

final class AbstractCrudRepositoryProxyUtil {

	private static final Method createMatPathChildrenUpdateQueryMethod;

	static {
		try {
			createMatPathChildrenUpdateQueryMethod = AbstractCrudRepository.class.getDeclaredMethod(
				"createMatPathChildrenUpdateQuery",
				Request.class, String.class, MaterializedPath.class, String.class, String.class);
		} catch (final NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public static Object invoke_createMatPathChildrenUpdateQuery(final Object proxy, final Object[] args) {
		try {
			return Proxy.getInvocationHandler(proxy).invoke(proxy, createMatPathChildrenUpdateQueryMethod, args);
		} catch (final Throwable e) {
			throw new RuntimeException(e);
		}
	}

}
