package br.com.dannemann.persistence;

import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.repository.CrudRepository;

public abstract class AbstractCrudRepository<E, P> implements CrudRepository<E, P> {

	// --------------------------------------------------------------------------
	// Fields:

	private final Class<E> entityType;
	private final Class<P> entityPrimaryKeyType;

	// -------------------------------------------------------------------------
	// Abstract interface:

	protected abstract Object getPersister(final String tenantID);

	// D framework features interface:

	abstract Object createMatPathChildrenUpdateQuery(
			final Request req, final String fieldName, final MaterializedPath matPathAnn, final String newMatPath, final String dbMatPath);

	// --------------------------------------------------------------------------
	// Constructors:

	public AbstractCrudRepository() {
//		final Object parameterizedTypeObj = getClass().getGenericSuperclass();
//
//		if (parameterizedTypeObj instanceof ParameterizedType) {
//			final ParameterizedType parameterizedType = (ParameterizedType) parameterizedTypeObj;
//
//			entityType = (Class<E>) parameterizedType.getActualTypeArguments()[0];
//			entityPrimaryKeyType = (Class<P>) parameterizedType.getActualTypeArguments()[1];
//		} else {
			entityType = null;
			entityPrimaryKeyType = null;
//		}
	}

	// --------------------------------------------------------------------------
	// Protected interface:

	// TODO: DO I NEED THIS HERE?
//	protected P getEntityID(final Request<E, P> req) {
//		final Object entityID = UtilEntity.getId(req.getEntity());
//
//		return entityID != null ? (P) entityID : null;
//	}

	// --------------------------------------------------------------------------
	// Getters and setters:

	public Class<E> getEntityType() {
		return entityType;
	}

	public Class<P> getEntityPrimaryKeyType() {
		return entityPrimaryKeyType;
	}

	// --------------------------------------------------------------------------
}
