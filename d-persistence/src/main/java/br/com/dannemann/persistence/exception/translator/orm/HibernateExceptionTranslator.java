package br.com.dannemann.persistence.exception.translator.orm;

import java.lang.reflect.Method;
import java.util.ResourceBundle;

import br.com.dannemann.core.SupportedLocales;
import br.com.dannemann.core.util.UtilString;
import br.com.dannemann.persistence.exception.DPersistenceException;

public final class HibernateExceptionTranslator {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Static:

	// -------------------------------------------------------------------------
	// Fields:

	private static final String MESSAGE_BUNDLE_FILE_NAME = "MessagesBundleHibernate";
	private static final String org_hibernate_PKG = "org.hibernate";
	private static final String org_hibernate_PropertyValueException = "org.hibernate.PropertyValueException";
	private static final String PropertyValueException_BUNDLE_KEY = "propertyValueException";

	private static ResourceBundle BUNDLE_en_US;
	private static ResourceBundle BUNDLE_pt_BR;

	private static final Class<?>[] DUMMY_CLASS_ARR = new Class<?>[0];
	private static final Object[] DUMMY_OBJ_ARR = new Object[0];

	// -------------------------------------------------------------------------
	// Translation:

	/**
	 * Translate exceptions from org.hibernate.**.* package.
	 */
	public static DPersistenceException translate(final Throwable t, final String localeTag) {
		final RuntimeException hibernateException = searchForHibernateExceptionCause(t);

		if (hibernateException != null) {
			final String exceptionClassName = hibernateException.getClass().getName();

			DPersistenceException transDetails = null;

			if (exceptionClassName.equals(org_hibernate_PropertyValueException))
				transDetails = translatePropertyValueException(hibernateException, localeTag);

			return transDetails;
		} else
			return null;
	}

	// Specific translators:

	/**
	 * Translates org.hibernate.PropertyValueException.
	 */
	private static DPersistenceException translatePropertyValueException(final RuntimeException propertyValueException, final String localeTag) {
		try {
			final Class<?> propertyValueExceptionClass = propertyValueException.getClass();

			final Method getEntityNameMethod = propertyValueExceptionClass.getMethod("getEntityName", DUMMY_CLASS_ARR);
			final String entityName = (String) getEntityNameMethod.invoke(propertyValueException, DUMMY_OBJ_ARR);

			final Method getPropertyNameMethod = propertyValueExceptionClass.getMethod("getPropertyName", DUMMY_CLASS_ARR);
			final String propertyName = (String) getPropertyNameMethod.invoke(propertyValueException, DUMMY_OBJ_ARR);

			final String message = getFromBundle(PropertyValueException_BUNDLE_KEY, localeTag);

			final DPersistenceException translationDetails = new DPersistenceException(message, propertyValueException);
			translationDetails.setCode(PropertyValueException_BUNDLE_KEY);
			translationDetails.setObjectName(entityName);
			translationDetails.setField(propertyName);

			return translationDetails;
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DPersistenceException("TODO", "ERRO AO TRADUZIR EXCECAO HIBERNATE org.hibernate.PropertyValueException");
		}
	}

	// -------------------------------------------------------------------------
	// Utilities

	// Public:

	public static boolean isHibernateException(final Throwable t) {
		return t.getClass().getName().startsWith(org_hibernate_PKG);
	}

	public static RuntimeException searchForHibernateExceptionCause(final Throwable t) {
		if (t == null)
			return null;
		else if (isHibernateException(t))
			return (RuntimeException) t;
		else
			return searchForHibernateExceptionCause(t.getCause());
	}

	// Private:

	private static String getFromBundle(final String bundleKey, final String localeTag) {
		String message = null;

		if (SupportedLocales.is_pt_BR(localeTag))
			message = getBundle_pt_BR().getString(bundleKey);
		else
			message = getBundle_en_US().getString(bundleKey);

		if (UtilString.isEmpty(message))
			throw new DPersistenceException("TODO", "COULD NOT RETRIEVE MESSAGE FROM HIBERNATE BUNDLE.. EXCEPTION TRANSLATOR");

		return message;
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	private static ResourceBundle getBundle_en_US() {
		if (BUNDLE_en_US == null)
			BUNDLE_en_US = ResourceBundle.getBundle(MESSAGE_BUNDLE_FILE_NAME, SupportedLocales.LOCALE_en_US);

		return BUNDLE_en_US;
	}

	private static ResourceBundle getBundle_pt_BR() {
		if (BUNDLE_pt_BR == null)
			BUNDLE_pt_BR = ResourceBundle.getBundle(MESSAGE_BUNDLE_FILE_NAME, SupportedLocales.LOCALE_pt_BR);

		return BUNDLE_pt_BR;
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Instance:

	// Constructors:

	private HibernateExceptionTranslator() {
	}

	// -------------------------------------------------------------------------
}
