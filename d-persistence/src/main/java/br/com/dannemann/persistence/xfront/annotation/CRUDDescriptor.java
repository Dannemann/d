package br.com.dannemann.persistence.xfront.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CRUDDescriptor {

	public String classNameFormatted() default "";
	public String menuNode() default "";
	public String menuIcon() default "";
	public boolean useButtonSearch() default true;
	public boolean useButtonSearchAll() default true;
	public boolean useButtonInsertNewRow() default true;
	public boolean useButtonSaveOrUpdate() default true;
	public boolean useButtonExclude() default true;
	public int numberOfGridColumnsOnForm() default 2;
	public String tabs() default "";
	public String usingAddressForm() default "";
	public String fileUploaders() default "";
	public String dataGridColumns() default "*";
	public String executeAfterSave() default "";
	public boolean searchAllOnOpen() default false;
	public boolean cachedCRUDForm() default false;
	public String customCRUDForm() default "";
	public boolean manyToManyAssocTable() default false;

}
