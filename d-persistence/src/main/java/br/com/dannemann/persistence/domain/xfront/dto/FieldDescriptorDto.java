package br.com.dannemann.persistence.domain.xfront.dto;

public class FieldDescriptorDto {

	//--------------------------------------------------------------------------
	// Fields:

	private int index;
	private int tabNavigatorIndex;
	private int length;
	private String defaultValue;
	private String acceptableValues;
	private String fieldNameFormatted;
	private String postLabel;
	private String dataGridColumn;
	private String dataGridColumnWidth;
	private String solDescriptonField;
	private String solAutoCompleteMode;
	private String solFieldsToSelectCSV;
	private String solDataGridColumnsCSV;
	private String solDataGridColumnsToRemoveCSV;
	private String solDataGridColumnsManyToOneCSV;
	private String solLikeDefaults;
	private String searchFieldFor;
	private String fkHolderFor;
	private String hierarchicalCodeIdField;
	private String hierarchicalCodeParentField;
	private String compositeIDFields;
	private String customADGLabelFunction;
	private String mask;
	private String imageField;
	private String groupByThisField;
	private String manyToManyTarget;
	private String manyToManyTableName;
	private String manyToManyJoinColumnName;
	private String manyToManyInverseJoinColumnName;
	
	
	
	private boolean requiredForSearch;
	private boolean bypassRequiredFieldsForSearch;
	private byte minLengthForSearch;
	
	
	
	private boolean isMainID;
	private boolean isCodeField;
	private boolean isDescriptionField;
	private boolean isNullable;
	private boolean isReadonly;
	private boolean isInsertable;
	private boolean isInsertableDisabled;
	private boolean isUpdatable;
	private boolean isUnique;
	private boolean isDateField;
	private boolean isHourField;
	private boolean isTimestamp;
	private boolean isCEP;
	private boolean isCNPJ;
	private boolean isCPF;
	private boolean isGender;
	private boolean isPasswordField;
	private boolean isPasswordEncrypted;
	private boolean isPhoneNumber;
	private boolean isManyToOne;
	private boolean isVersionField;
	private boolean isRemarksField;
	private boolean validateCEPNumber;
	private boolean validateCNPJNumber;
	private boolean validateCPFNumber;
	private boolean validatePhoneNumber;

	// Just on bean:

	private boolean isOneToMany;
	private String oneToManyTarget;
	private String oneToManyMappedBy;

	private boolean isManyToMany;

	// Auto filled.

	private String javaField;
	private String javaType;
	private String solClassName;

	//--------------------------------------------------------------------------
	// Getters and setters:

	// Field descriptor indicator:

	public boolean getIsFieldDescriptor() {
		return true;
	}

	public void setIsFieldDescriptor(boolean isFieldDescriptor) {
	}

	// Getters and setters:

	public int getIndex() {
		return this.index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getTabNavigatorIndex() {
		return this.tabNavigatorIndex;
	}

	public void setTabNavigatorIndex(int tabNavigatorIndex) {
		this.tabNavigatorIndex = tabNavigatorIndex;
	}

	public int getLength() {
		return this.length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getAcceptableValues() {
		return this.acceptableValues;
	}

	public void setAcceptableValues(String acceptableValues) {
		this.acceptableValues = acceptableValues;
	}

	public String getFieldNameFormatted() {
		return this.fieldNameFormatted;
	}

	public void setFieldNameFormatted(String fieldNameFormatted) {
		this.fieldNameFormatted = fieldNameFormatted;
	}

	public String getPostLabel() {
		return postLabel;
	}

	public void setPostLabel(String postLabel) {
		this.postLabel = postLabel;
	}

	public String getDataGridColumn() {
		return this.dataGridColumn;
	}

	public void setDataGridColumn(String dataGridColumn) {
		this.dataGridColumn = dataGridColumn;
	}

	public String getDataGridColumnWidth() {
		return dataGridColumnWidth;
	}

	public void setDataGridColumnWidth(String dataGridColumnWidth) {
		this.dataGridColumnWidth = dataGridColumnWidth;
	}

	public String getSolDescriptonField() {
		return this.solDescriptonField;
	}

	public void setSolDescriptonField(String solDescriptonField) {
		this.solDescriptonField = solDescriptonField;
	}

	public String getSolAutoCompleteMode() {
		return solAutoCompleteMode;
	}

	public void setSolAutoCompleteMode(String solAutoCompleteMode) {
		this.solAutoCompleteMode = solAutoCompleteMode;
	}

	public String getSolFieldsToSelectCSV() {
		return this.solFieldsToSelectCSV;
	}

	public void setSolFieldsToSelectCSV(String solFieldsToSelectCSV) {
		this.solFieldsToSelectCSV = solFieldsToSelectCSV;
	}

	public String getSolDataGridColumnsCSV() {
		return this.solDataGridColumnsCSV;
	}

	public void setSolDataGridColumnsCSV(String solDataGridColumnsCSV) {
		this.solDataGridColumnsCSV = solDataGridColumnsCSV;
	}

	public String getSolDataGridColumnsToRemoveCSV() {
		return this.solDataGridColumnsToRemoveCSV;
	}

	public void setSolDataGridColumnsToRemoveCSV(String solDataGridColumnsToRemoveCSV) {
		this.solDataGridColumnsToRemoveCSV = solDataGridColumnsToRemoveCSV;
	}

	public String getSolDataGridColumnsManyToOneCSV() {
		return this.solDataGridColumnsManyToOneCSV;
	}

	public void setSolDataGridColumnsManyToOneCSV(String solDataGridColumnsManyToOneCSV) {
		this.solDataGridColumnsManyToOneCSV = solDataGridColumnsManyToOneCSV;
	}

	public String getSolLikeDefaults() {
		return solLikeDefaults;
	}

	public void setSolLikeDefaults(String solLikeDefaults) {
		this.solLikeDefaults = solLikeDefaults;
	}

	public String getSearchFieldFor() {
		return this.searchFieldFor;
	}

	public void setSearchFieldFor(String searchFieldFor) {
		this.searchFieldFor = searchFieldFor;
	}

	public String getFkHolderFor() {
		return this.fkHolderFor;
	}

	public void setFkHolderFor(String fkHolderFor) {
		this.fkHolderFor = fkHolderFor;
	}

	public String getHierarchicalCodeIdField() {
		return this.hierarchicalCodeIdField;
	}

	public void setHierarchicalCodeIdField(String hierarchicalCodeIdField) {
		this.hierarchicalCodeIdField = hierarchicalCodeIdField;
	}

	public String getHierarchicalCodeParentField() {
		return this.hierarchicalCodeParentField;
	}

	public void setHierarchicalCodeParentField(String hierarchicalCodeParentField) {
		this.hierarchicalCodeParentField = hierarchicalCodeParentField;
	}

	public String getCompositeIDFields() {
		return this.compositeIDFields;
	}

	public void setCompositeIDFields(String compositeIDFields) {
		this.compositeIDFields = compositeIDFields;
	}

	public String getCustomADGLabelFunction() {
		return customADGLabelFunction;
	}

	public void setCustomADGLabelFunction(String customADGLabelFunction) {
		this.customADGLabelFunction = customADGLabelFunction;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	public String getImageField() {
		return imageField;
	}

	public void setImageField(String imageField) {
		this.imageField = imageField;
	}

	public String getGroupByThisField() {
		return groupByThisField;
	}

	public void setGroupByThisField(String groupByThisField) {
		this.groupByThisField = groupByThisField;
	}



	public String getManyToManyTarget() {
		return manyToManyTarget;
	}

	public void setManyToManyTarget(String manyToManyTarget) {
		this.manyToManyTarget = manyToManyTarget;
	}

	public String getManyToManyTableName() {
		return manyToManyTableName;
	}

	public void setManyToManyTableName(String manyToManyTableName) {
		this.manyToManyTableName = manyToManyTableName;
	}

	public String getManyToManyJoinColumnName() {
		return manyToManyJoinColumnName;
	}

	public void setManyToManyJoinColumnName(String manyToManyJoinColumnName) {
		this.manyToManyJoinColumnName = manyToManyJoinColumnName;
	}

	public String getManyToManyInverseJoinColumnName() {
		return manyToManyInverseJoinColumnName;
	}

	public void setManyToManyInverseJoinColumnName(String manyToManyInverseJoinColumnName) {
		this.manyToManyInverseJoinColumnName = manyToManyInverseJoinColumnName;
	}




	public boolean isRequiredForSearch() {
		return requiredForSearch;
	}

	public void setRequiredForSearch(final boolean requiredForSearch) {
		this.requiredForSearch = requiredForSearch;
	}
	
	public boolean isBypassRequiredFieldsForSearch() {
		return bypassRequiredFieldsForSearch;
	}

	public void setBypassRequiredFieldsForSearch(final boolean bypassRequiredFieldsForSearch) {
		this.bypassRequiredFieldsForSearch = bypassRequiredFieldsForSearch;
	}
	
	public byte getMinLengthForSearch() {
		return minLengthForSearch;
	}

	public void setMinLengthForSearch(final byte minLengthForSearch) {
		this.minLengthForSearch = minLengthForSearch;
	}
	
	
	
	
	
	
	
	
	
	
	

	public boolean getIsMainID() {
		return this.isMainID;
	}

	public void setIsMainID(boolean isMainID) {
		this.isMainID = isMainID;
	}

	public boolean getIsCodeField() {
		return isCodeField;
	}

	public void setIsCodeField(boolean isCodeField) {
		this.isCodeField = isCodeField;
	}

	public boolean getIsDescriptionField() {
		return this.isDescriptionField;
	}

	public void setIsDescriptionField(boolean isDescriptionField) {
		this.isDescriptionField = isDescriptionField;
	}

	public boolean getIsNullable() {
		return this.isNullable;
	}

	public void setIsNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	public boolean getIsReadonly() {
		return this.isReadonly;
	}

	public void setIsReadonly(boolean isReadonly) {
		this.isReadonly = isReadonly;
	}

	public boolean getIsInsertable() {
		return this.isInsertable;
	}

	public void setIsInsertable(boolean isInsertable) {
		this.isInsertable = isInsertable;
	}

	public boolean getIsInsertableDisabled() {
		return isInsertableDisabled;
	}

	public void setIsInsertableDisabled(boolean isInsertableDisabled) {
		this.isInsertableDisabled = isInsertableDisabled;
	}

	public boolean getIsUpdatable() {
		return this.isUpdatable;
	}

	public void setIsUpdatable(boolean isUpdatable) {
		this.isUpdatable = isUpdatable;
	}

	public boolean getIsUnique() {
		return isUnique;
	}

	public void setIsUnique(boolean isUnique) {
		this.isUnique = isUnique;
	}

	public boolean getIsDateField() {
		return this.isDateField;
	}

	public void setIsDateField(boolean isDateField) {
		this.isDateField = isDateField;
	}

	public boolean getIsHourField() {
		return this.isHourField;
	}

	public void setIsHourField(boolean isHourField) {
		this.isHourField = isHourField;
	}

	public boolean getIsTimestamp() {
		return this.isTimestamp;
	}

	public void setIsTimestamp(boolean isTimestamp) {
		this.isTimestamp = isTimestamp;
	}

	public boolean getIsCEP() {
		return this.isCEP;
	}

	public void setIsCEP(boolean isCEP) {
		this.isCEP = isCEP;
	}

	public boolean getIsCNPJ() {
		return this.isCNPJ;
	}

	public void setIsCNPJ(boolean isCNPJ) {
		this.isCNPJ = isCNPJ;
	}

	public boolean getIsCPF() {
		return this.isCPF;
	}

	public void setIsCPF(boolean isCPF) {
		this.isCPF = isCPF;
	}

	public boolean getIsPasswordField() {
		return this.isPasswordField;
	}

	public void setIsPasswordField(boolean isPasswordField) {
		this.isPasswordField = isPasswordField;
	}

	public boolean getIsPasswordEncrypted() {
		return this.isPasswordEncrypted;
	}

	public void setIsPasswordEncrypted(boolean isPasswordEncrypted) {
		this.isPasswordEncrypted = isPasswordEncrypted;
	}

	public boolean getIsGender() {
		return this.isGender;
	}

	public void setIsGender(boolean isGender) {
		this.isGender = isGender;
	}

	public boolean getIsPhoneNumber() {
		return this.isPhoneNumber;
	}

	public void setIsPhoneNumber(boolean isPhoneNumber) {
		this.isPhoneNumber = isPhoneNumber;
	}

	public boolean getIsManyToOne() {
		return this.isManyToOne;
	}

	public void setIsManyToOne(boolean isManyToOne) {
		this.isManyToOne = isManyToOne;
	}

	public boolean getIsVersionField() {
		return this.isVersionField;
	}

	public void setIsVersionField(boolean isVersionField) {
		this.isVersionField = isVersionField;
	}

	public boolean isValidateCNPJNumber() {
		return this.validateCNPJNumber;
	}

	public boolean getIsRemarksField() {
		return isRemarksField;
	}

	public void setIsRemarksField(boolean isRemarksField) {
		this.isRemarksField = isRemarksField;
	}

	public boolean isValidateCEPNumber() {
		return validateCEPNumber;
	}

	public void setValidateCEPNumber(boolean validateCEPNumber) {
		this.validateCEPNumber = validateCEPNumber;
	}

	public void setValidateCNPJNumber(boolean validateCNPJNumber) {
		this.validateCNPJNumber = validateCNPJNumber;
	}

	public boolean isValidateCPFNumber() {
		return this.validateCPFNumber;
	}

	public void setValidateCPFNumber(boolean validateCPFNumber) {
		this.validateCPFNumber = validateCPFNumber;
	}

	public boolean isValidatePhoneNumber() {
		return validatePhoneNumber;
	}

	public void setValidatePhoneNumber(boolean validatePhoneNumber) {
		this.validatePhoneNumber = validatePhoneNumber;
	}

	public boolean getIsOneToMany() {
		return this.isOneToMany;
	}

	public void setIsOneToMany(boolean isOneToMany) {
		this.isOneToMany = isOneToMany;
	}

	public String getOneToManyTarget() {
		return oneToManyTarget;
	}

	public void setOneToManyTarget(String oneToManyTarget) {
		this.oneToManyTarget = oneToManyTarget;
	}

	public String getOneToManyMappedBy() {
		return oneToManyMappedBy;
	}

	public void setOneToManyMappedBy(String oneToManyMappedBy) {
		this.oneToManyMappedBy = oneToManyMappedBy;
	}

	public boolean getIsManyToMany() {
		return isManyToMany;
	}

	public void setIsManyToMany(boolean isManyToMany) {
		this.isManyToMany = isManyToMany;
	}

	// Auto filled.

	public String getJavaField() {
		return this.javaField;
	}

	public void setJavaField(String javaField) {
		this.javaField = javaField;
	}

	public String getJavaType() {
		return this.javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getSolClassName() {
		return this.solClassName;
	}

	public void setSolClassName(String solClassName) {
		this.solClassName = solClassName;
	}

	//--------------------------------------------------------------------------
}
