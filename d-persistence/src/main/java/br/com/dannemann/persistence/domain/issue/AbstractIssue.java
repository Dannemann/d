package br.com.dannemann.persistence.domain.issue;

public abstract class AbstractIssue {

	// -------------------------------------------------------------------------
	// Fields:

	public static final String ARG = ":arg";

	// -------------------------------------------------------------------------
	// Public static interface:

	public static final String get(String error, final String... args) {
		final int argsQtd = args.length;
		for (int i = 1; i <= argsQtd; i++)
			error = error.replace(ARG.concat(Integer.toString(i)), args[i - 1]);

		return error;
	}

	// -------------------------------------------------------------------------
}
