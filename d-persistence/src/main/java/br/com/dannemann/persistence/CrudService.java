package br.com.dannemann.persistence;

import static br.com.dannemann.core.util.UtilReflect.invoke;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.dannemann.core.ThreadRegistry;
import br.com.dannemann.core.util.UtilBean;
import br.com.dannemann.core.util.UtilString;
import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.domain.DInternalParams;
import br.com.dannemann.persistence.domain.issue.DErrors;
import br.com.dannemann.persistence.domain.materialized_path.MaterializedPathBean;
import br.com.dannemann.persistence.domain.materialized_path.exception.ChildOfItselfException;
import br.com.dannemann.persistence.domain.materialized_path.exception.MaterializedPathException;
import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.dto.Response;
import br.com.dannemann.persistence.exception.DPersistenceException;
import br.com.dannemann.persistence.repository.CrudRepository;
import br.com.dannemann.persistence.util.UtilEntity;

public abstract class CrudService<E, P> implements br.com.dannemann.persistence.service.CrudService<E, P> {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Abstract interface:

	protected abstract CrudRepository<E, P> getRepository();

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// CrudService overrides:

	// -------------------------------------------------------------------------
	// Select:

	@Override
	public Response findById(final Request req) {
		validateRequest(req);
		return addResultToResponse(getRepository().findById(req));
	}
	
	@Override
	public Response findByExample(final Request req) {
		return addResultToResponse(getRepository().findByExample(req));
	}

	@Override
	public Response findAll(final Request req) {
		return addResultToResponse(getRepository().findAll(req));
	}

	
	
	
	
	
	
	protected Response addResultToResponse(final Object result) {
		final Response response = new Response();
		response.setResult(result);

		return response;
	}

	// -------------------------------------------------------------------------
	// Insert/Update:

	@Override
	public Response save(final Request req) {
		validateRequest(req);
		initializeRequest(req);

		final Response response = new Response();

		try {
			response.setResult(executeSave(req));
		} catch (final ChildOfItselfException e) {
//			response.addIssue(new Issue(e));
		}

		return response;
	}

	private E executeSave(final Request req) throws ChildOfItselfException {
		handleTreeAlgorithms(req, true);

		final E savedEntity = getRepository().save(req);

		handleTreeAlgorithms(req, false);

		return savedEntity;
	}

	// -------------------------------------------------------------------------
	// Delete:

	@Override
	public Response delete(final Request req) {
		validateRequest(req);
//		initializeRequest(req);

		final E toDelete = getRepository().findById(req);

		req.setObject(toDelete);

		getRepository().delete(req);

		return new Response();
	}

	// -------------------------------------------------------------------------
	// Execute:

	@Override
	public Response execute(final Request req) {
		validateRequest(req);
		return addResultToResponse(getRepository().execute(req));
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Request handling:

	/**
	 * <p><b>Important:</b> This method must be the only one used for checking if an object is brand new or not.
	 * This way we encapsulate this verification process in case we need to change it in the future. 
	 * @param req The incoming {@link br.com.dannemann.persistence.dto.Request Request} object.
	 * @return Whether the incoming object is brand new or not.
	 */
	protected boolean checkIfBrandNewEntity(final Request req) {
		final UtilEntity upo = new UtilEntity(req.getObject());

		if (upo.isSinglePrimaryKey())
			return upo.hasPopulatedPrimaryKeys();
		if (upo.isCompositePrimaryKey())
			throw new DPersistenceException("TODO", "NOT IMPLEMENTED YET.");
		else if (upo.hasNoPrimaryKey())
			throw new DPersistenceException("TODO", "NOT IMPLEMENTED YET.");
		else
			throw new DPersistenceException("TODO", "NÃO PODE ACONTECER ESSE ERRO.");
	}

	protected void validateRequest(final Request req) {
		if (req == null)
			throw new DPersistenceException("10010", DErrors.REQUEST_NULL);
	}

	protected void initializeRequest(final Request req) {
		ThreadRegistry.setObjectName(req.getObjectType());
		ThreadRegistry.setLocaleTag(req.getLocaleTag());

		if (!req.containsKey(DParams.IS_BRAND_NEW_ENTITY))
			req.put(DParams.IS_BRAND_NEW_ENTITY, checkIfBrandNewEntity(req));
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Tree algorithms handling:

	protected void handleTreeAlgorithms(final Request req, final boolean beforeInsertOrUpdate) throws ChildOfItselfException {
		handleMatPaths(req, beforeInsertOrUpdate);
	}

	// -------------------------------------------------------------------------
	// Materialized path:

	// Business:

	protected void handleMatPaths(final Request req, final boolean beforeInsertOrUpdate) throws ChildOfItselfException {
		final MaterializedPathBean materializedPathBean = new MaterializedPathBean(req);

		if (materializedPathBean.hasMatPath())
			updateMaterializedPathChildren(req, materializedPathBean, beforeInsertOrUpdate);
	}

	// TODO: SEPARATE THIS PROCESS IN METHODS BEFORE AND AFTER THE PERSIST ACTION.
	protected void updateMaterializedPathChildren(final Request req, final MaterializedPathBean matPathBean, final boolean beforeInsertOrUpdate) throws ChildOfItselfException {
		final Object e = req.getObject();
		final List<Method> entityMatPathMethods = matPathBean.getMatPathMethods();

		// If it is a brand new object being saved.
		if (matPathBean.isBrandNewEntity()) {
			if (!beforeInsertOrUpdate) {
				// Iterating over all mat. path methods in the incoming object.
				for (final Method method : entityMatPathMethods) {
					final MaterializedPath matPathAnn = method.getAnnotation(MaterializedPath.class); // The @MaterializedPath annotated in the object field/method.
					final String fieldName = getAndCheckMatPathFieldName(req, matPathAnn, method); // Getting and checking the field name.

					matPathBean.checkPadding(fieldName, matPathAnn);

					// Calling the mat. path setter updating the object.
					invoke(
						UtilBean.getSetMethodFromGet(method, e), // TODO: Is this the better way to get the setter?
						e,
						matPathBean.addPaddedID(matPathAnn, matPathBean.getAndCheckIfString(fieldName, invoke(method, e))));
				}

				getRepository().save(req);
			}
		} else {
			if (beforeInsertOrUpdate) {
				final ArrayList<Object> childrenUpdateQueries = new ArrayList<Object>();
				final HashMap<Method, ChildrenUpdaterDTO> dtoMap = new HashMap<Method, ChildrenUpdaterDTO>();

				// Iterating over all mat. path methods in the incoming object for constraints validation.
				for (final Method method : entityMatPathMethods) {
					final MaterializedPath matPathAnn = method.getAnnotation(MaterializedPath.class); // The @MaterializedPath annotated in the object field/method.
					final String fieldName = getAndCheckMatPathFieldName(req, matPathAnn, method); // Getting and checking the field name.

					matPathBean.checkPadding(fieldName, matPathAnn);

					// Retrieving the new mat. path that the user selected in the UI and the current mat. path in the database.
					final String newMatPath = matPathBean.getAndCheckIfString(fieldName, invoke(method, e));
					final String dbMatPath = matPathBean.getDBMatPaths().get(fieldName);

					matPathBean.checkIfIsChildOfItself(fieldName, matPathAnn, newMatPath, dbMatPath);

					// Filling the DTO that will be used to easily retrieve the values (for performance) that will be persisted.
					dtoMap.put(method, new ChildrenUpdaterDTO(fieldName, matPathAnn, newMatPath, dbMatPath));
				}

				for (final Method method : entityMatPathMethods) {
					final ChildrenUpdaterDTO dto = dtoMap.get(method);

					// In this "if" we check if there is any errors with the new mat. path that could lead to abortion of the process. 
					// After this "if" clause, we will be sure that both mat. paths are different and need update on children.
					if (!dto.dbMatPath.equals(dto.newMatPath)) {
						final String finalMatPath = matPathBean.addPaddedID(dto.matPathAnn, dto.newMatPath);
	
						// Calling the mat. path setter updating the Java object.
						invoke(UtilBean.getSetMethodFromGet(method, e), e, finalMatPath);
	
//						if (!MaterializedPathBean.isSameParent(dto.matPathAnn, dto.newMatPath, dto.dbMatPath))
						if (!dto.dbMatPath.equals(finalMatPath)) {
							if (getRepository() instanceof Proxy)
								childrenUpdateQueries.add(
									AbstractCrudRepositoryProxyUtil.invoke_createMatPathChildrenUpdateQuery(getRepository(), new Object[] {
										req,
										dto.fieldName,
										dto.matPathAnn,
										finalMatPath,
										dto.dbMatPath }));
							else if (getRepository() instanceof AbstractCrudRepository)
								childrenUpdateQueries.add(
									((AbstractCrudRepository<E, P>) getRepository()).createMatPathChildrenUpdateQuery(
										req,
										dto.fieldName,
										dto.matPathAnn,
										finalMatPath,
										dto.dbMatPath));
							else
								throw new RuntimeException("NO PROXY AND NO ABSTRACT CRUD REPOSITORY."); // TODO: WORK ON THIS.
						}
					}
				}

				req.put(DInternalParams.MPATH_CHILD_UPDATE_QUERIES, childrenUpdateQueries);
			} else
				getRepository().execute(req);
//			getRepository().execute(
//					(List<Object>) req.getParams().get(DInternalParams.MPATH_CHILD_UPDATE_QUERIES));
		}
	}

	// Utilities:

	public static String getAndCheckMatPathFieldName(final Request req, final MaterializedPath matPathAnn, final Method method) {
		String fieldName = matPathAnn.fieldName();

		if (UtilString.isEmpty(fieldName))
			fieldName = UtilBean.getFieldNameFromGetSet(method.getName()); // Convention over configuration.

		if (UtilString.isEmpty(fieldName))
			throw new MaterializedPathException("10012", DErrors.MPATH_COULDNT_RETRIEVE_FIELD_NAME, req);

		return fieldName;
	}

	// -------------------------------------------------------------------------
}

class ChildrenUpdaterDTO {

	public final String fieldName;
	public final MaterializedPath matPathAnn;
	public final String newMatPath;
	public final String dbMatPath;

	ChildrenUpdaterDTO(final String fieldName, final MaterializedPath matPathAnn, final String newMatPath, final String dbMatPath) {
		this.fieldName = fieldName;
		this.matPathAnn = matPathAnn;
		this.newMatPath = newMatPath;
		this.dbMatPath = dbMatPath;
	}

}
