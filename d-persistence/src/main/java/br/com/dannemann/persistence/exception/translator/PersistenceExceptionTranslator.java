package br.com.dannemann.persistence.exception.translator;

import br.com.dannemann.core.SupportedLocales;
import br.com.dannemann.persistence.exception.DPersistenceException;
import br.com.dannemann.persistence.exception.translator.jdbc.SQLExceptionTranslator;
import br.com.dannemann.persistence.exception.translator.orm.HibernateExceptionTranslator;

public final class PersistenceExceptionTranslator {

	// -------------------------------------------------------------------------
	// Static:

	// Translation:

	public static DPersistenceException translate(final Throwable t, final String localeTag) {
		// java.sql.SQLException (and children) translation.
		final DPersistenceException sqlTranslationDetails = SQLExceptionTranslator.translate(t, localeTag);
		if (sqlTranslationDetails != null)
			return sqlTranslationDetails;

		// Hibernate translation.
		final DPersistenceException hibernateTranslationDetails = HibernateExceptionTranslator.translate(t, localeTag);
		if (hibernateTranslationDetails != null)
			return hibernateTranslationDetails;

		return null;
	}

	public static DPersistenceException translate(final Throwable t) {
		return translate(t, SupportedLocales.currentLocale());
	}

	// -------------------------------------------------------------------------
	// Instance:

	// Constructors:

	private PersistenceExceptionTranslator() {
	}

	// -------------------------------------------------------------------------
}
