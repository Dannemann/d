package br.com.dannemann.d.persistence.hibernate3.model;

import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.HibernateException;
import org.hibernate.event.MergeEvent;
import org.hibernate.event.MergeEventListener;
import org.hibernate.event.PersistEvent;
import org.hibernate.event.PersistEventListener;
import org.hibernate.event.PreInsertEvent;
import org.hibernate.event.PreInsertEventListener;
import org.hibernate.event.PreUpdateEvent;
import org.hibernate.event.PreUpdateEventListener;
import org.hibernate.event.SaveOrUpdateEvent;
import org.hibernate.event.SaveOrUpdateEventListener;

@MappedSuperclass
public abstract class DEntityUid extends DEntity {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Static:

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// -------------------------------------------------------------------------
	// Inner classes:

	/**
	 * UUID generator for Hibernate 3 (javax.persistence.@EntityListeners are not supported by Hibernate 3).
	 */
//	For Spring's org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean:
//	----------------------------------------------------------------------------------------
//	...
//	<bean id="uidGeneratorListener" class="br.com.dannemann.persistence.hibernate3.model.DEntityUid.UIDGeneratorListener" />
//	
//	<bean id="sessionFactory" class="org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean">
//		...
//		<property name="eventListeners">
//            <map>
//                <entry key="create"><ref bean="uidGeneratorListener" /></entry>
//                <entry key="pre-insert"><ref bean="uidGeneratorListener" /></entry>
//                <entry key="pre-update"><ref bean="uidGeneratorListener" /></entry>
//                <entry key="save"><ref bean="uidGeneratorListener" /></entry>
//                <entry key="save-update"><ref bean="uidGeneratorListener" /></entry>
//                <entry key="update"><ref bean="uidGeneratorListener" /></entry>
//                <entry key="merge"><ref bean="uidGeneratorListener" /></entry>
//            </map>
//		</property>
//		...
//	</bean>
//	...
	public static class UIDGeneratorListener
	implements PreInsertEventListener, PreUpdateEventListener, PersistEventListener, SaveOrUpdateEventListener, MergeEventListener {

		private static final long serialVersionUID = 1L;

		public UIDGeneratorListener() {
		}

		private void execPopulateUUID(final Object entity) {
			if (entity instanceof DEntityUid)
				((DEntityUid) entity).populateUUID();
		}

		@Override
		public boolean onPreInsert(final PreInsertEvent event) {
			execPopulateUUID(event.getEntity());
			return false;
		}

		@Override
		public boolean onPreUpdate(final PreUpdateEvent event) {
			execPopulateUUID(event.getEntity());
			return false;
		}

		@Override
		public void onPersist(final PersistEvent event) throws HibernateException {
			execPopulateUUID(event.getObject());
		}

		@Override
		public void onPersist(final PersistEvent event, @SuppressWarnings("rawtypes") final Map createdAlready) throws HibernateException {
			execPopulateUUID(event.getObject());
		}

		@Override
		public void onSaveOrUpdate(final SaveOrUpdateEvent event) throws HibernateException {
			execPopulateUUID(event.getEntity());
		}

		@Override
		public void onMerge(final MergeEvent event) throws HibernateException {
			execPopulateUUID(event.getEntity());
		}

		@Override
		public void onMerge(final MergeEvent event, @SuppressWarnings("rawtypes") final Map copiedAlready) throws HibernateException {
			execPopulateUUID(event.getEntity());
		}

	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Instance:

	// -------------------------------------------------------------------------
	// Fields:

	private String uuid;

	// -------------------------------------------------------------------------
	// Constructors:

	public DEntityUid() {
	}

	// -------------------------------------------------------------------------
	// UUID generation:

	private String populateUUID() {
		if (uuid == null)
			uuid = UUID.randomUUID().toString();

		return uuid;
	}

	// -------------------------------------------------------------------------
	// Object overrides:

	@Override
	public int hashCode() {
		return populateUUID().hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		return (o == this || (o instanceof DEntityUid && populateUUID().equals(((DEntityUid) o).populateUUID())));
	}

	@Override
	public String toString() {
		return new StringBuilder(getClass().getName()).append(" [id=").append(getId()).append(", version=").append(version).append(", uuid=").append(uuid).append("]").toString();
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	// "UUID" and "UID" are Oracle reserved keywords, so, we end up with "uuidz" as column name.
	@Column(name = "uuidz", unique = true, nullable = false, updatable = false, length = 36)
	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	// -------------------------------------------------------------------------
}
