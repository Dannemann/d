package br.com.dannemann.d.persistence.hibernate3.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import br.com.dannemann.persistence.model.AbstractEntity;

@MappedSuperclass
public abstract class DEntity extends AbstractEntity {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	protected int version;

	// -------------------------------------------------------------------------
	// Constructors:

	public DEntity() {
	}

	// -------------------------------------------------------------------------
	// Abstract methods:

	@Transient
	public abstract Serializable getId();

	// -------------------------------------------------------------------------
	// Getters and setters:

	@Transient
	public int getVersion() {
		return version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	// -------------------------------------------------------------------------
	// Object overrides:

	@Override
	public int hashCode() {
		return 31 * 1 + getId().hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		if (getId() != ((DEntity) obj).getId())
			return false;

		return true;
	}

	@Override
	public String toString() {
		return new StringBuilder(getClass().getName()).append(" [id=").append(getId()).append(", version=").append(version).append("]").toString();
	}

	// -------------------------------------------------------------------------
}
