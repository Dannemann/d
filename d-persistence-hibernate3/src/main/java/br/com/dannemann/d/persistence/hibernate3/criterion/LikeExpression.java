// Copied from Hibernate 3.6.10.Final.

package br.com.dannemann.d.persistence.hibernate3.criterion;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.TypedValue;

/**
 * A criterion representing a "like" expression
 *
 * @author Scott Marlow
 * @author Steve Ebersole
 */
public class LikeExpression implements Criterion {
	private final String propertyName;
	private final Object value;
	private final Character escapeChar;
	private final boolean ignoreCase;

// D framework features BEGIN

	// Diacriticless search: https://forum.hibernate.org/viewtopic.php?f=1&t=970231

	private boolean diacriticlessSearch;

	protected LikeExpression(final String propertyName, final String value, final MatchMode matchMode, final Character escapeChar, final boolean ignoreCase, boolean diacriticlessSearch) {
		this(propertyName, matchMode.toMatchString(value), escapeChar, ignoreCase);

		this.diacriticlessSearch = diacriticlessSearch;
	}

// END

	protected LikeExpression(
			String propertyName,
			String value,
			Character escapeChar,
			boolean ignoreCase) {
		this.propertyName = propertyName;
		this.value = value;
		this.escapeChar = escapeChar;
		this.ignoreCase = ignoreCase;
	}

	protected LikeExpression(
			String propertyName,
			String value) {
		this( propertyName, value, null, false );
	}

	protected LikeExpression(
			String propertyName,
			String value,
			MatchMode matchMode) {
		this( propertyName, matchMode.toMatchString( value ) );
	}

	protected LikeExpression(
			String propertyName,
			String value,
			MatchMode matchMode,
			Character escapeChar,
			boolean ignoreCase) {
		this( propertyName, matchMode.toMatchString( value ), escapeChar, ignoreCase );
	}

// D framework features BEGIN

	// Original method.
//	public String toSqlString(
//			Criteria criteria,
//			CriteriaQuery criteriaQuery) throws HibernateException {
//		Dialect dialect = criteriaQuery.getFactory().getDialect();
//		String[] columns = criteriaQuery.findColumns(propertyName, criteria);
//		if ( columns.length != 1 ) {
//			throw new HibernateException( "Like may only be used with single-column properties" );
//		}
//		String lhs = ignoreCase
//				? dialect.getLowercaseFunction() + '(' + columns[0] + ')'
//	            : columns[0];
//		return lhs + " like ?" + ( escapeChar == null ? "" : " escape \'" + escapeChar + "\'" );
//
//	}

	public String toSqlString(final Criteria criteria, final CriteriaQuery criteriaQuery) throws HibernateException {
		final Dialect dialect = criteriaQuery.getFactory().getDialect();
		final String[] columns = criteriaQuery.findColumns(propertyName, criteria);

		if (columns.length != 1)
			throw new HibernateException("Like may only be used with single-column properties");

		if (diacriticlessSearch) {
			final String lhs = ignoreCase ? "translate("+dialect.getLowercaseFunction()+'('+columns[0]+"), "+AssociationExample.TRANSLATE+")" : "translate("+columns[0]+", "+AssociationExample.TRANSLATE+")";
			return lhs + " like translate(?, "+AssociationExample.TRANSLATE+')' + (escapeChar == null ? "" : " escape \'" + escapeChar + "\'");
		} else {
			final String lhs = ignoreCase ? dialect.getLowercaseFunction() + '(' + columns[0] + ')' : columns[0];
			return lhs + " like ?" + (escapeChar == null ? "" : " escape \'" + escapeChar + "\'");
		}
	}

// END

	public TypedValue[] getTypedValues(
			Criteria criteria,
			CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[] {
				criteriaQuery.getTypedValue( criteria, propertyName, ignoreCase ? value.toString().toLowerCase() : value.toString() )
		};
	}
}
