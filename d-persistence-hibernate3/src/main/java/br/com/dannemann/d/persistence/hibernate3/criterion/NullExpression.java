package br.com.dannemann.d.persistence.hibernate3.criterion;

public class NullExpression extends org.hibernate.criterion.NullExpression {

	protected NullExpression(final String propertyName) {
		super(propertyName);
	}

}
