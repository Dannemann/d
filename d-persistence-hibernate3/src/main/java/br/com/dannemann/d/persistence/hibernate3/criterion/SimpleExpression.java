package br.com.dannemann.d.persistence.hibernate3.criterion;

public class SimpleExpression extends org.hibernate.criterion.SimpleExpression {

	protected SimpleExpression(final String propertyName, final Object value, final String op, final boolean ignoreCase) {
		super(propertyName, value, op, ignoreCase);
	}

}
