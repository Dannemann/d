package br.com.dannemann.core.util;

public final class DUtilClass {

	public static String getSimpleNameFromComplete(final String completeClassName) {
		return completeClassName.substring(completeClassName.lastIndexOf(".") + 1);
	}

}
