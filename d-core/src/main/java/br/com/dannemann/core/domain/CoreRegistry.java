package br.com.dannemann.core.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import br.com.dannemann.core.log.DLogger;

// TODO: i18n.
// TODO: Could this class be a Spring bean or something like that? Replace it for a mature thread-safe implementation.
// Singleton reference implementation: https://gist.github.com/aritraroy/76300e7fb5582e8a79e93ba3b48e193d
public final class CoreRegistry implements Serializable, Cloneable {

	// -------------------------------------------------------------------------
	// Fields:

	private static final DLogger LOGGER = new DLogger(CoreRegistry.class);

	private static final long serialVersionUID = 1L;

	// -------------------------------------------------------------------------
	// Singleton:

	private static volatile Map<String, Map<String, Object>> registry;

	private CoreRegistry() {
		if (registry != null)
			throw new RuntimeException("Cannot instantiate a Singleton object using its constructor. Use its \"getInstance()\" method.");
	}

	public static Map<String, Map<String, Object>> getInstance() {
		// Implementing double-locking to prevent ambiguity in multi-threaded environment.
		if (registry == null)
			synchronized (CoreRegistry.class) {
				if (registry == null)
					registry = new HashMap<String, Map<String, Object>>();
			}

		return registry;
	}

	public Object readResolve() {
		return getInstance();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	// -------------------------------------------------------------------------
	// Registry.

	public static void addApplication(final String appId) {
		if (getInstance().containsKey(appId))
			LOGGER.warn(new StringBuilder(CoreRegistry.class.getSimpleName()).append(": Reseting \"").append(appId).append("\" application core registry.").toString());

		getInstance().put(appId, new HashMap<String, Object>());
	}

	public static void addApplicationProperty(final String appId, final String propertyName, final Object propertyValue) throws CoreRegistryUpdateException {
		if (getInstance().containsKey(appId))
			getInstance().get(appId).put(propertyName, propertyValue);
		else
			throw new CoreRegistryUpdateException(
				new StringBuilder("Could not find \"").append(appId).append("\" application in the ").append(CoreRegistry.class.getSimpleName())
				.append(". Property \"").append(propertyName).append("\" with value \"").append(propertyValue).append("\" was not set.").toString());
	}

	public static Map<String, Object> getApplicationRegistry(final String appId) throws CoreRegistryUpdateException {
		if (getInstance().containsKey(appId))
			return getInstance().get(appId);
		else
			throw new CoreRegistryUpdateException(
				new StringBuilder("Could not find \"").append(appId).append("\" application in the ").append(CoreRegistry.class.getSimpleName()).toString());
	}

	public static Object getApplicationProperty(final String appId, final String propertyName) throws CoreRegistryUpdateException {
		return getApplicationRegistry(appId).get(propertyName);
	}

	// -------------------------------------------------------------------------
}
