package br.com.dannemann.core;

import java.util.Locale;

import br.com.dannemann.core.util.UtilString;

public final class SupportedLocales {

	public static final String LOCALETAG_en_US = "en_US";
	public static final String LOCALETAG_pt_BR = "pt_BR";

	public static final Locale LOCALE_en_US = new Locale("en", "US");
	public static final Locale LOCALE_pt_BR = new Locale("pt", "BR");

	public static String currentLocale() {
		String localeTag = ThreadRegistry.getLocaleTag();

		if (UtilString.isEmpty(localeTag))
			localeTag = Locale.getDefault().toString();

		return localeTag;
	}

	public static boolean is_pt_BR(final String localeTag) {
		return LOCALETAG_pt_BR.equalsIgnoreCase(localeTag) || "BR".equalsIgnoreCase(localeTag);
	}

	private SupportedLocales() {
	}

}
