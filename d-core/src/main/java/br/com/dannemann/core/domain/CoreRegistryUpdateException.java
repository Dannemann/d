package br.com.dannemann.core.domain;

/**
 * <p>The core registry data is very sensitive. As this is a checked exception, 
 * we guarantee that the developer knows exactly what he is doing when modifying it.</p>
 */
public final class CoreRegistryUpdateException extends Exception {

	private static final long serialVersionUID = 1L;

	public CoreRegistryUpdateException() {
	}

	public CoreRegistryUpdateException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CoreRegistryUpdateException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CoreRegistryUpdateException(final String message) {
		super(message);
	}

	public CoreRegistryUpdateException(final Throwable cause) {
		super(cause);
	}

}
