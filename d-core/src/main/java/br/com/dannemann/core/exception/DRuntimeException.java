package br.com.dannemann.core.exception;

public class DRuntimeException extends RuntimeException implements Issue {

	// --------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private String code;
	private String objectName;
	private String field;
	private String value;
	private Object source;

	// --------------------------------------------------------------------------
	// Constructors:

	public DRuntimeException(final String message) {
		super(message);
	}

	public DRuntimeException(final Throwable cause) {
		super(cause);
	}

	public DRuntimeException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DRuntimeException(final String code, final String message) {
		this(message);

		this.code = code;
	}

	public DRuntimeException(final String code, final String message, final Object source) {
		this(code, message);

		this.source = source;
	}

	// --------------------------------------------------------------------------
	// Issue implementations:

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(final String code) {
		this.code = code;
	}

	@Override
	public String getObjectName() {
		return objectName;
	}

	@Override
	public void setObjectName(final String objectName) {
		this.objectName = objectName;
	}

	@Override
	public String getField() {
		return field;
	}

	@Override
	public void setField(final String field) {
		this.field = field;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public Object getSource() {
		return source;
	}

	@Override
	public void setSource(final Object source) {
		this.source = source;
	}

	// --------------------------------------------------------------------------
}
