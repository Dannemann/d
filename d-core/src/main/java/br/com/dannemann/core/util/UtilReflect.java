package br.com.dannemann.core.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

// TODO: Should I java.lang.invoke.MethodHandle instead normal reflection on this case? Analyze with nano time benchmarks.
public final class UtilReflect {

	// getMethod / getMethods:

	public static Method getMethod(final String methodName, final Class<?>[] parameters, final Object o) {
		try {
			return o.getClass().getMethod(methodName, parameters);
		} catch (final NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null; // TODO: Throw new customized exception. The system must stop here.
	}

	public static Method getMethod(final String methodName, final Object o) {
		return getMethod(methodName, new Class<?>[]{}, o);
	}

	public static Method[] getMethods(final Object o, final boolean declaredMethods) {
		return declaredMethods ? o.getClass().getDeclaredMethods() : o.getClass().getMethods();
	}

	public static Method[] getMethods(final Object o) {
		return getMethods(o, true);
	}

	// getMethodsByAnnotation:

	public static List<Method> getMethodsByAnnotation(final Class<? extends Annotation> annotationClass, final Object o, final boolean searchDeclaredMethods) {
		final List<Method> foundMethods = new ArrayList<Method>();

		for (final Method method : getMethods(o, searchDeclaredMethods))
			if (method.isAnnotationPresent(annotationClass))
				foundMethods.add(method);

		return foundMethods;
	}

	public static List<Method> getMethodsByAnnotation(final Class<? extends Annotation> annotationClass, final Object o) {
		return getMethodsByAnnotation(annotationClass, o, true);
	}

	// invoke:

	public static Object invoke(final Method method, final Object target, final Object... args) {
		try {
			return method.invoke(target, args);
		} catch (final IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null; // TODO: Throw new customized exception. The system must stop here.
	}

	public static Object invoke(final String methodName, final Object target, final Object... args) {
		return invoke(getMethod(methodName, target), target, args);
	}

}
