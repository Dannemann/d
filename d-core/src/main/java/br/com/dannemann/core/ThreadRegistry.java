package br.com.dannemann.core;

public final class ThreadRegistry {

	// -------------------------------------------------------------------------
	// Fields:

	private static ThreadLocal<String> objectName = new ThreadLocal<String>();
	private static ThreadLocal<String> localeTag = new ThreadLocal<String>();

	// -------------------------------------------------------------------------
	// Getters and setters:

	public static String getObjectName() {
		return objectName.get();
	}

	public static void setObjectName(final String objectName) {
		ThreadRegistry.objectName.set(objectName);
	}

	public static String getLocaleTag() {
		return localeTag.get();
	}

	public static void setLocaleTag(final String localeTag) {
		ThreadRegistry.localeTag.set(localeTag);
	}

	// -------------------------------------------------------------------------
}
