package br.com.dannemann.core.util;

import java.util.ArrayList;

public final class DUtilString {

	public static String addEndChar(final String character, String targetString) {
		targetString = targetString.trim();

		if (targetString.endsWith(character))
			return targetString;
		else
			return (new StringBuilder()).append(targetString).append(character).toString();
	}

	public static String camelCaseFieldToNormalWithSpaces(String getOrSetMethodName) {
		final int stringLength = getOrSetMethodName.length();
		final StringBuilder finalString = new StringBuilder();
		char character;

		for (int i = 0; i < stringLength; i++) {
			character = getOrSetMethodName.charAt(i);

			if (Character.isUpperCase(character))
				finalString.append(" ").append(String.valueOf(character).toLowerCase());
			else
				finalString.append(character);
		}

		return DUtilString.upperCaseFirstLetter(finalString.toString().trim());
	}

	public static String camelCaseToTextWithSpaces(String getOrSetMethodName) {
		return DUtilString.camelCaseFieldToNormalWithSpaces(DUtilString.removeGetSetPrefix(getOrSetMethodName));
	}

	public static int containsWord(String word, String targetStr, boolean caseSensitive) {
		if (!caseSensitive) {
			word = (new StringBuilder()).append(" ").append(word.trim().toLowerCase()).append(" ").toString();
			targetStr = (new StringBuilder()).append(" ").append(targetStr.trim().toLowerCase()).append(" ").toString();
		}

		final int wordBeginIndex = targetStr.indexOf(word);

		if (wordBeginIndex == -1)
			return -1;

		if (String.valueOf(targetStr.charAt(wordBeginIndex)).equals(" "))
			return wordBeginIndex + 1;

		return wordBeginIndex;
	}

	public static ArrayList<String> csvToArrayListOfStrings(final String csv) {
		final String[] csvSplitted = csv.split(",");
		final ArrayList<String> values = new ArrayList<String>();

		for (final String value : csvSplitted)
			values.add(value.trim());

		return values;
	}

	public static String fieldNameToGetter(String fieldName) {
		return (new StringBuilder()).append("get").append(DUtilString.upperCaseFirstLetter(fieldName)).toString();
	}

	public static String fieldNameToSetter(String fieldName) {
		return (new StringBuilder()).append("set").append(DUtilString.upperCaseFirstLetter(fieldName)).toString();
	}

	public static String fullClassNameToSimpleClassName(String fullClassName) {
		return fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
	}

	public static String getterSettterToFieldName(String getOrSetMethodName) {
		return DUtilString.lowerCaseFirstLetter(removeGetSetPrefix(getOrSetMethodName));
	}

	public static String lowerCaseFirstLetter(String string) {
		return (new StringBuilder()).append(string.substring(0, 1).toLowerCase()).append(string.substring(1)).toString();
	}

	public static String upperCaseFirstLetter(String string) {
		return (new StringBuilder()).append(string.substring(0, 1).toUpperCase()).append(string.substring(1)).toString();
	}

	public static boolean hasValue(String string) {
		return string != null && !string.trim().equals("");
	}

	public static boolean hasValue(Object string) {
		return string != null && !string.toString().trim().equals("");
	}

	public static String removeGetSetPrefix(String getOrSetMethodName) {
		return getOrSetMethodName.substring(3, getOrSetMethodName.length());
	}

	public static String removeSpecialChars(String string) {
        return string.replaceAll("[^0-9a-zA-Z ]", "");
	}

	public static String replaceAccents(String string) {
		return string
			.replaceAll("[�����]", "a")
			.replaceAll("[����]", "e")
			.replaceAll("[����]", "i")
			.replaceAll("[�����]", "o")
			.replaceAll("[����]", "u")
			.replaceAll("[�����]", "A")
			.replaceAll("[����]", "E")
			.replaceAll("[����]", "I")
			.replaceAll("[�����]", "O")
			.replaceAll("[����]", "U")
			.replace('�', 'c')
			.replace('�', 'C')
			.replace('�', 'n')
			.replace('�', 'N');
	}

	public static String replaceAccentsRemoveSpecialCharsAndUpperCase(String string) {
		return removeSpecialChars(replaceAccents(string)).toUpperCase();
	}

}
