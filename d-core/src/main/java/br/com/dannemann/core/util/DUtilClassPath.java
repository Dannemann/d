package br.com.dannemann.core.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public final class DUtilClassPath {

	public static void addFileToClassPath(String file) throws IOException {
		addFileToClassPath(new File(file));
	}

	public static void addFileToClassPath(File file) throws MalformedURLException {
		addURLToClassPath(file.toURI().toURL());
	}

	public static void addURLToClassPath(URL url) {
		try {
			Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
			method.setAccessible(true);
			method.invoke((URLClassLoader) ClassLoader.getSystemClassLoader(), new Object[] { url });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
