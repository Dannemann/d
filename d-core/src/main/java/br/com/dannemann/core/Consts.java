package br.com.dannemann.core;

import java.util.regex.Pattern;

public final class Consts {


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// TODO: Spend some more time on these patterns just to double check.
	public static final Pattern REGEXP_getter = Pattern.compile("get[A-Z_\\$][A-Za-z\\d_\\$]*");
//	public static final Pattern REGEXP_setter = Pattern.compile("set[A-Z_\\$][A-Za-z\\d_\\$]*");
//	public static final Pattern REGEXP_getter_setter = Pattern.compile("[gs]et[A-Z_\\$][A-Za-z\\d_\\$]*");

	public static final String PREFIX_LOG = " ### ";

	public static final String P6SPY_Prepared_SQL = "Prepared SQL: ";

	public static final String CHAR_bar = "|";
	public static final String CHAR_dot = ".";
	public static final String CHAR_dquote = "\"";
	public static final String CHAR_tab = "\t";
	public static final String CHAR_2tabs = "\t\t";
	public static final String CHAR_empty = "";
	public static final String CHAR_space = " ";

	public static final String g = "g";
	public static final String get = "get";
	public static final String s = "s";
	public static final String set = "set";

	public static void main(String[] args) {
		String a = "a";
		String b = "a";
		String c = new String("a");
		
		System.out.println(a == b);
		System.out.println(a == c);
		
	}
	
}
