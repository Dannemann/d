package br.com.dannemann.core.pojo;

import java.util.HashMap;

/**
 * This class is a map. You can extend it to create your own request and add your specific request getters and setters.
 */
public class Request extends HashMap<String, Object> {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	// Generic request properties:

	public static final String tenant = "tenant";
	public static final String object = "object";
	public static final String objectType = "objectType";
	public static final String localeTag = "localeTag";

	// -------------------------------------------------------------------------
	// Getters and setters:

	public String getTenant() {
		return (String) get(tenant);
	}

	public void setTenant(final String tenant) {
		put(Request.tenant, tenant);
	}

	public Object getObject() {
		return get(object);
	}

	public void setObject(final Object object) {
		put(Request.object, object);
	}

	public String getObjectType() {
		return (String) get(objectType);
	}

	public void setObjectType(final String objectType) {
		put(Request.objectType, objectType);
	}

	public String getLocaleTag() {
		return (String) get(localeTag);
	}

	public void setLocaleTag(final String locale) {
		put(Request.localeTag, locale);
	}

	// -------------------------------------------------------------------------
	// Builders:

	public static Request build() {
		return new Request();
	}

	public Request withTenant(final String tenant) {
		setTenant(tenant);
		return this;
	}

	public Request withObject(final Object object) {
		setObject(object);
		return this;
	}

	public Request withObjectType(final String objectType) {
		setObjectType(objectType);
		return this;
	}

	public Request withLocaleTag(final String localeTag) {
		setLocaleTag(localeTag);
		return this;
	}

	public Request withKeyValue(final String key, final Object value) {
		put(key, value);
		return this;
	}

	// -------------------------------------------------------------------------
}
