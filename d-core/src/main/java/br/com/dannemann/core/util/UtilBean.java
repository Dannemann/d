package br.com.dannemann.core.util;

import static br.com.dannemann.core.Consts.REGEXP_getter;
import static br.com.dannemann.core.Consts.g;
import static br.com.dannemann.core.Consts.get;
import static br.com.dannemann.core.Consts.s;
import static br.com.dannemann.core.Consts.set;

import java.lang.reflect.Method;

public final class UtilBean {

	public static String getFieldNameFromGetSet(final String getterOrSetter) {
		return getterOrSetter.startsWith(get) || getterOrSetter.startsWith(set) ? UtilString.lowerCaseFirstChar(getterOrSetter.substring(3)) : null;
	}

	public static String getFieldNameFromGetSet(final Method getterOrSetter) {
		return getFieldNameFromGetSet(getterOrSetter.getName());
	}

	/**
	 * Concatenates a given {@code prefix} with {@code fieldName}, upper casing {@code fieldName}'s first char.
	 * @param prefix A prefix like for example "has", "get", "set", "add" and etc.
	 * @param fieldName An attribute (instance variable) name.
	 * @return The {@code prefix} concatenated to {@code fieldName} with the first char in upper case ("hasName", "getAddress" for example).
	 */
	public static String getCamelCasedName(final String prefix, final String fieldName) {
		return prefix.concat(UtilString.upperCaseFirstChar(fieldName));
	}

	public static String getGetterName(final String fieldName) {
		return isGetter(fieldName) ? fieldName : getCamelCasedName(get, fieldName);
	}

	public static String getSetterName(final String attrName) {
		return attrName.startsWith(set) ? attrName : getCamelCasedName(set, attrName);
	}

	public static String getGetterNameFromSetter(final String setterName) {
		return setterName.startsWith(set) ? g.concat(setterName.substring(1)) : setterName;
	}

	public static String getGetterNameFromSetter(final Method setterMethod) {
		return getGetterNameFromSetter(setterMethod.getName());
	}

	public static Method getGetterMethodFromSetter(final Method setterMethod, final Object o) {
		try {
			return o.getClass().getMethod(getGetterNameFromSetter(setterMethod), setterMethod.getReturnType());
		} catch (final NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null; // TODO: Throw new customized exception. The system must stop here.
	}

	public static String getSetterNameFromGetter(final String getterName) {
		return s.concat(getterName.substring(1));
	}

	public static String getSetterNameFromGetter(final Method getterMethod) {
		return getSetterNameFromGetter(getterMethod.getName());
	}

	public static Method getSetMethodFromGet(final Method getterMethod, final Object o) {
		try {
			return o.getClass().getMethod(getSetterNameFromGetter(getterMethod), getterMethod.getReturnType());
		} catch (final NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null; // TODO: Throw new customized exception. The system must stop here.
	}

	public static Object invokeGetter(final String fieldName, final Object target) {
		return UtilReflect.invoke(getGetterName(fieldName), target);
	}

	// isGetter:

	public static boolean isGetter(final String methodName) {
		return REGEXP_getter.matcher(methodName).matches();
	}

	public static boolean isGetter(final Method method) {
		return isGetter(method.getName());
	}

	public static boolean isNotGetter(final Method method) {
		return !isGetter(method);
	}

}
