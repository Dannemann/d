package br.com.dannemann.core.comparator;

import java.util.Comparator;

public class ClassSimpleNameComparator implements Comparator<Class<?>> {

	@Override
	public int compare(final Class<?> o1, final Class<?> o2) {
		return o1.getSimpleName().compareToIgnoreCase(o2.getSimpleName());
	}

}
