package br.com.dannemann.core.util;

import java.util.List;

public final class UtilList {

	public static boolean isEmpty(final List<?> list) {
		return list == null || list.isEmpty();
	}

	public static boolean isNotEmpty(final List<?> list) {
		return !isEmpty(list);
	}

}
