package br.com.dannemann.core.util;

import java.io.File;
import java.util.ArrayList;

public final class DUtilFile {

	public static String addEndSlash(String path) {
		if (path.endsWith("/"))
			return path;
		else
			return (new StringBuilder()).append(path).append("/").toString();
	}

	public static String removeEndSlash(String path) {
		if (path.endsWith("/"))
			return path.substring(0, path.length() - 1);
		else
			return path;
	}

	public static ArrayList<File> listFolders(final File file) {
		final ArrayList<File> folders = new ArrayList<File>();

		for (final File subFile : file.listFiles())
			if (subFile.isDirectory())
				folders.add(subFile);

		return folders;
	}

	public static String listFoldersNamesCSV(final File file, final String removeOnFileName) {
		final StringBuilder folders = new StringBuilder();

		for (final File subFile : file.listFiles())
			if (subFile.isDirectory()) {
				if (DUtilString.hasValue(removeOnFileName))
					folders.append(subFile.getName().replace(removeOnFileName, "")).append(",");
				else
					folders.append(subFile.getName()).append(",");
			}

		final String csvWithLastComma = folders.toString();

		return csvWithLastComma.substring(0, csvWithLastComma.length() - 1);
	}

	public static String listFoldersNamesCSV(final File file) {
		return listFoldersNamesCSV(file, null);
	}

}
