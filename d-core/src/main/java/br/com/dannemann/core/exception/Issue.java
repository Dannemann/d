package br.com.dannemann.core.exception;

public interface Issue {

	String getCode();
	void setCode(final String code);

	String getObjectName();
	void setObjectName(final String objectName);

	String getField();
	void setField(final String field);

	String getValue();
	void setValue(final String value);

	String getMessage();
//	void setMessage(final String value); // Exceptions doesn't have this method.

	Object getSource();
	void setSource(final Object source);

}
