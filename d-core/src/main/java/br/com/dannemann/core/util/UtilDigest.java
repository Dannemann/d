package br.com.dannemann.core.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.dannemann.core.exception.DRuntimeException;

public final class UtilDigest {

	private static final MessageDigest md5;

	static {
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (final NoSuchAlgorithmException e) {
			throw new DRuntimeException(e);
		}
	}

	public static MessageDigest getMD5() {
		return md5;
	}

	public static String digestMD5(final String string) {
		return new BigInteger(1, md5.digest(string.getBytes())).toString(16);
	}

}
