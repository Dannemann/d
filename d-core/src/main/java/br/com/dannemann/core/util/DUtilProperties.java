package br.com.dannemann.core.util;

import java.io.IOException;
import java.util.Properties;

public final class DUtilProperties {

	public static Properties loadFromClasspath(final String fileName) throws IOException {
		final Properties properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName));
		return properties;
	}

}
