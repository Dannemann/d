package br.com.dannemann.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class DUtilStringTransformer {

	public static ArrayList<Map<String, Object>> transformation1IntoArrayListOfMaps(final String type1String) {
		final ArrayList<Map<String, Object>> finalArrayList = new ArrayList<Map<String,Object>>();

		for (final String item : type1String.split(";;")) {
			final Map<String, Object> itemMap = new HashMap<String, Object>();

			for (final String property : item.split(";")) {
				final String[] propertyAndValue = property.split("=");

				if (propertyAndValue.length == 2)
					itemMap.put(propertyAndValue[0], propertyAndValue[1]);
				else if (propertyAndValue.length == 1)
					itemMap.put(propertyAndValue[0], null);
			}

			finalArrayList.add(itemMap);
		}

		return finalArrayList;
	}

}
