package br.com.dannemann.core.log;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DLogger {

	// -------------------------------------------------------------------------
	// Fields:

	private String logPrefix = "########## ";
	private String logSufix = "";

	private final Log log;

	// -------------------------------------------------------------------------
	// Constructor:

	public DLogger(final Class<?> clazz) {
		log = LogFactory.getLog(clazz);
	}

	// -------------------------------------------------------------------------
	// Logging:

	public void fatal(final String msg) {
		log.fatal(createLogString(msg));
	}

	public void error(final String msg) {
		log.error(createLogString(msg));
	}

	public void warn(final String msg) {
		log.warn(createLogString(msg));
	}

	public void info(final String msg) {
		log.info(createLogString(msg));
	}

	public void debug(final String msg) {
		log.debug(createLogString(msg));
	}

	public void trace(final String msg) {
		log.trace(createLogString(msg));
	}
	// -------------------------------------------------------------------------
	// Utilities:

	protected String createLogString(final String msg) {
		return new StringBuilder(logPrefix).append(msg).append(logSufix).toString();
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	public String getLogPrefix() {
		return logPrefix;
	}

	public void setLogPrefix(final String logPrefix) {
		this.logPrefix = logPrefix;
	}

	public String getLogSufix() {
		return logSufix;
	}

	public void setLogSufix(final String logSufix) {
		this.logSufix = logSufix;
	}

	public Log getLog() {
		return log;
	}

	// -------------------------------------------------------------------------
}
