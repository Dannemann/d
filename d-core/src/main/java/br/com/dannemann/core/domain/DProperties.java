package br.com.dannemann.core.domain;

import java.io.IOException;
import java.util.Properties;

import br.com.dannemann.core.log.DLogger;
import br.com.dannemann.core.util.DUtilProperties;

// TODO: i18n.
public class DProperties {

	// -------------------------------------------------------------------------
	// Fields:

	private static final DLogger LOGGER = new DLogger(DProperties.class);

	public static final String D_PROPERTIES_DEFAULT_FILE_NAME = "d.properties";

	// -------------------------------------------------------------------------
	// Singleton:

	private static Properties dProperties;

	public static Properties getInstance() {
		if (dProperties == null)
			try {
				dProperties = DUtilProperties.loadFromClasspath(D_PROPERTIES_DEFAULT_FILE_NAME);

				LOGGER.info(new StringBuilder("\"").append(D_PROPERTIES_DEFAULT_FILE_NAME).append("\" file loaded successfully.").toString());
			} catch (final IOException e) {
				LOGGER.info(new StringBuilder("\"").append(D_PROPERTIES_DEFAULT_FILE_NAME).append("\" file not found.").toString());
			}

		return dProperties;
	}

	// -------------------------------------------------------------------------
}
