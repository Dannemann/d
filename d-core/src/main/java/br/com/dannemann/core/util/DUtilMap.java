package br.com.dannemann.core.util;

import java.util.Iterator;
import java.util.Map;

public final class DUtilMap {

	public static void dumpMap(final Map<?, ?> map) {
		final Iterator<?> it = map.entrySet().iterator();

		while (it.hasNext()) {
			final Map.Entry<?, ?> pairs = (Map.Entry<?, ?>) it.next();

			System.out.println(pairs.getKey() + " = " + pairs.getValue());
		}
	}

}
