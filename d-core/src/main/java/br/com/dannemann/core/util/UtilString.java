package br.com.dannemann.core.util;

import java.util.regex.Pattern;

public final class UtilString {



	public static String getFirstChar(final String targetStr) {
		return targetStr.substring(0, 1);
	}

	public static String getFirstCharLower(final String targetStr) {
		return getFirstChar(targetStr).toLowerCase();
	}

	public static String getFirstCharUpper(final String targetStr) {
		return getFirstChar(targetStr).toUpperCase();
	}

	public static String lowerCaseFirstChar(final String targetStr) {
		return new StringBuilder(getFirstCharLower(targetStr)).append(targetStr.substring(1)).toString();
	}

	public static String upperCaseFirstChar(final String targetStr) {
		return new StringBuilder(getFirstCharUpper(targetStr)).append(targetStr.substring(1)).toString();
	}

	
	
	
	
	public static boolean isEmpty(final Object obj) {
		return obj == null || ((String) obj).isEmpty();
	}

	public static boolean isEmpty(final String str) {
		return str == null || str.isEmpty();
	}

	public static boolean isNotEmpty(final String targetStr) {
		return !isEmpty(targetStr);
	}

	public static boolean isNumeric(final String str) {
		try {
			Double.parseDouble(str);
		} catch (final Exception e) {
			return false;
		}

		return true;
	}

	
	
	
	
	
	public static String removeAll(final String toRemove, final String targetStr) {
		return replaceAll(toRemove, "", targetStr);
	}

	public static String replaceAll(final String oldStr, final String newStr, final String targetStr) {
		return targetStr.replaceAll(Pattern.quote(oldStr), newStr);
	}

	public static String[] splitEveryIndex(final int everyIndex, final String targetStr) {
		final int loopTime = targetStr.length() / everyIndex;
		final String[] splittedStr = new String[loopTime];

		for (int i = 0; i < loopTime; i++) {
			final int begin = i * everyIndex;
			splittedStr[i] = targetStr.substring(begin, begin + everyIndex);
		}

		return splittedStr;
	}


	
	
	
	
	
	
	// TODO: Put this in a utils class later.
	public static String padLeftWithZeroes(final Object value, final int size) {
		try {
			if (value == null) 
				return null; // TODO: Work on this.

			// Parsing considering that ID is a number. Trying this first for performance.
			return String.format(new StringBuilder("%").append("0").append(size).append("d").toString(), Long.parseLong(value.toString()));
		} catch (final Exception e) {
			// TODO: Work on this.
			e.printStackTrace();
		}

		// TODO: Implement another way to padding in case of fail above.

		return null;
	}
	
	
	
	
	
	
	
	
	
	
	public static String addSeparators(final String separator, final int everyIndex, final String targetStr) {
		final String[] targetSplitted = splitEveryIndex(everyIndex, targetStr);
		final StringBuilder finalString = new StringBuilder();

		String tmpDelimiter = "";

		for (final String targetPiece : targetSplitted) {
			finalString.append(tmpDelimiter).append(targetPiece);
			tmpDelimiter = separator;
		}

		return finalString.toString();
	}

	

}
