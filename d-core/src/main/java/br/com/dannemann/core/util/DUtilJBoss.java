package br.com.dannemann.core.util;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public final class DUtilJBoss {

	public InitialContext getJNDIDefaultInitialContext() {
		final Properties props = new Properties();
		props.put(InitialContext.PROVIDER_URL, "localhost:1099");
		props.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		props.put(InitialContext.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		try {
			return new InitialContext(props);
		} catch (NamingException e) {
			e.printStackTrace();
		}

		return null;
	}

	// TODO: FINISH THIS. COMPILE ERROR.
	@SuppressWarnings("unchecked")
	public String[] getAllDeployedDataSources(InitialContext lookupContext) throws Exception {
//		final Set<ServerObjectInstance> serverObjectInstances = ((MBeanServerConnection) lookupContext.lookup("jmx/invoker/RMIAdaptor")).queryMBeans(new ObjectName("jboss.jca:service=DataSourceBinding,*"), null);
//		final ArrayList<String> allDatasourcesArrayList = new ArrayList<String>();
//
//		for (ServerObjectInstance serverObjInstance : serverObjectInstances)
//			allDatasourcesArrayList.add(serverObjInstance.getObjectName().getKeyProperty("name")) ;
//
//		if (allDatasourcesArrayList.contains("DefaultDS"))
//			allDatasourcesArrayList.remove("DefaultDS");
//		if (allDatasourcesArrayList.contains("DAS"))
//			allDatasourcesArrayList.remove("DAS");
//
//		return allDatasourcesArrayList.toArray(new String[allDatasourcesArrayList.size()]);
		return null;
	}

}
