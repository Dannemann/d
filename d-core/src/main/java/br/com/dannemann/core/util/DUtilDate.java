package br.com.dannemann.core.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public final class DUtilDate {

	public static Map<String, String> now() {
		final Calendar c = new GregorianCalendar();
		c.setTime(new Date());

		final Map<String, String> dateNowMap = new HashMap<String, String>();
		dateNowMap.put("day", addAditionalZeroAtLeftIfNeeded(String.valueOf(c.get(Calendar.DAY_OF_MONTH))));
		dateNowMap.put("month", addAditionalZeroAtLeftIfNeeded(String.valueOf(c.get(Calendar.MONTH) + 1)));
		dateNowMap.put("year", String.valueOf(c.get(Calendar.YEAR)));
		dateNowMap.put("hour", addAditionalZeroAtLeftIfNeeded(String.valueOf(c.get(Calendar.HOUR_OF_DAY))));
		dateNowMap.put("minute", addAditionalZeroAtLeftIfNeeded(String.valueOf(c.get(Calendar.MINUTE))));
		dateNowMap.put("second", addAditionalZeroAtLeftIfNeeded(String.valueOf(c.get(Calendar.SECOND))));
		return dateNowMap;
	}

	public static String addAditionalZeroAtLeftIfNeeded(String dateData) {
		return dateData.length() == 1 ? (new StringBuilder()).append("0").append(dateData).toString() : dateData;
	}

	public static String nowMapToStringYYYYMMDD(final Map<String, String> dateNowMap) {
		return (new StringBuilder()).append(dateNowMap.get("year")).append(dateNowMap.get("month")).append(dateNowMap.get("day")).toString();
	}

	public static String nowMapToStringYYYYMMDDHHMM(final Map<String, String> dateNowMap) {
		return (new StringBuilder()).append(dateNowMap.get("year")).append(dateNowMap.get("month")).append(dateNowMap.get("day")).append(dateNowMap.get("hour")).append(dateNowMap.get("minute")).toString();
	}

}
