package br.com.dannemann.core.util;

import java.net.JarURLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;

public final class DUtilReflection {

	public static ClassLoader getClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}

	public static Class<?> loadClass(final String fullName)  {
		try {
			return getClassLoader().loadClass(fullName);
		} catch (final ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static List<Class<?>> loadClassesFromPkg(final String packageName) {
		try {
			final String packageNameSlashed = packageName.replace('.', '/');
			final Enumeration<JarEntry> entries = ((JarURLConnection) getClassLoader().getResources(packageNameSlashed).nextElement().openConnection()).getJarFile().entries();
			final ArrayList<Class<?>> loadedClasses = new ArrayList<>();

			String entryName;
			String strClazz;
			while (entries.hasMoreElements()) {
				entryName = entries.nextElement().getName();

				if (entryName.startsWith(packageNameSlashed) && entryName.endsWith(".class") && !entryName.contains("$") &&
					entryName.indexOf(packageNameSlashed) == 0 && !entryName.substring(packageNameSlashed.length() + 1).contains("/")) {
						strClazz = entryName.replace('/', '.');
						loadedClasses.add(Class.forName(strClazz.substring(0, strClazz.length() - 6)));
				}
			}

			return loadedClasses;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static List<Class<?>> loadClassesFromPkgs(final String[] packages) {
		final List<Class<?>> loadedClasses = new ArrayList<>();

		for (final String pkgName : packages)
			loadedClasses.addAll(loadClassesFromPkg(pkgName));

		return loadedClasses;
	}

}
