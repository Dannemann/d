package br.com.dannemann.core.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.dannemann.core.exception.DRuntimeException;

/**
 * @see <a href="https://viacep.com.br">https://viacep.com.br</a>
 */
public class ViaCEP {

	public static void main(String[] args) {
		find("38703036");
//		find("a");
//		find("1");
		find("11111111");
	}

	// -------------------------------------------------------------------------
	// Fields:

	private static final String baseUrl = "https://viacep.com.br/ws/";
	private static final String serviceReturnType = "json";

	private static final String cepJsonField = "cep";
	private static final String logradouroJsonField = "logradouro";
	private static final String complementoJsonField = "complemento";
	private static final String bairroJsonField = "bairro";
	private static final String localidadeJsonField = "localidade";
	private static final String ufJsonField = "uf";
	private static final String unidadeJsonField = "unidade";
	private static final String ibgeJsonField = "ibge";
	private static final String giaJsonField = "gia";

	private static final String cepJsonRegex = createJsonFieldValueRegex(cepJsonField, "\\d{5}\\-\\d{3}");
	private static final String logradouroJsonRegex = createJsonFieldValueRegex(logradouroJsonField, ".*");
	private static final String complementoJsonRegex = createJsonFieldValueRegex(complementoJsonField, ".*");
	private static final String bairroJsonRegex = createJsonFieldValueRegex(bairroJsonField, ".*");
	private static final String localidadeJsonRegex = createJsonFieldValueRegex(localidadeJsonField, ".*");
	private static final String ufJsonRegex = createJsonFieldValueRegex(ufJsonField, "[A-Z]{2}");
	private static final String unidadeJsonRegex = createJsonFieldValueRegex(unidadeJsonField, ".*");
	private static final String ibgeJsonRegex = createJsonFieldValueRegex(ibgeJsonField, ".*");
	private static final String giaJsonRegex = createJsonFieldValueRegex(giaJsonField, ".*");

	private static final String jsonAddressRegex = 
		"^\\{" +
		"\\s*" + cepJsonRegex + "," +
		"\\s*" + logradouroJsonRegex + "," +
		"\\s*" + complementoJsonRegex + "," +
		"\\s*" + bairroJsonRegex + "," +
		"\\s*" + localidadeJsonRegex + "," +
		"\\s*" + ufJsonRegex + "," +
		"\\s*" + unidadeJsonRegex + "," +
		"\\s*" + ibgeJsonRegex + "," +
		"\\s*" + giaJsonRegex +
		"\\}$";

	private static final Pattern jsonAddressPattern = Pattern.compile(jsonAddressRegex);
	private static final Pattern jsonAddressErrorPattern = Pattern.compile("^\\{\\s*\"erro\":\\s*true\\s*\\}$");

	// -------------------------------------------------------------------------
	// Public interface:

	public static Map<String, String> find(final String cep) {
		try {
			final URL url = new URL(getCompleteUrl(cep));
			final HttpURLConnection con = (HttpURLConnection) url.openConnection();

			final int responseCode = con.getResponseCode();
			if (responseCode != 200)
				throw new DRuntimeException("VIA CEP ERRO AO CONECTAR COM O SERVICO"); // TODO Auto-generated catch block

			final String response = readResponseStream(con.getInputStream());

			return jsonToMap(response);
		} catch (final IOException e) { // new URL(getCompleteUrl(cep));
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DRuntimeException("VIA CEP ERRO AO CONECTAR COM O SERVICO", e);
		}
	}

	// -------------------------------------------------------------------------
	// Private interface:

	private static String getCompleteUrl(final String cep) {
		return new StringBuilder(baseUrl).append(cep).append("/").append(serviceReturnType).toString();
	}

	private static String readResponseStream(final InputStream responseStream) throws IOException {
		final BufferedReader in = new BufferedReader(new InputStreamReader(responseStream));
		final StringBuilder response = new StringBuilder();

		String inputLine;
		while ((inputLine = in.readLine()) != null)
			response.append(inputLine);

		in.close();

		return response.toString();
	}

	private static String createJsonFieldValueRegex(final String field, final String value) {
		return new StringBuilder("\"").append(field).append("\":\\s*\"(").append(value).append(")\"").toString();
	}

	private static HashMap<String, String> jsonToMap(final String jsonStr) {
		final HashMap<String, String> addressData = new HashMap<String, String>();

		if (jsonAddressErrorPattern.matcher(jsonStr).matches()) // If no results found...
			return addressData;

		final Matcher m = jsonAddressPattern.matcher(jsonStr);
		if (m.matches()) {
			addressData.put(cepJsonField, m.group(1));
			addressData.put(logradouroJsonField, m.group(2));
			addressData.put(complementoJsonField, m.group(3));
			addressData.put(bairroJsonField, m.group(4));
			addressData.put(localidadeJsonField, m.group(5));
			addressData.put(ufJsonField, m.group(6));
			addressData.put(unidadeJsonField, m.group(7));
			addressData.put(ibgeJsonField, m.group(8));
			addressData.put(giaJsonField, m.group(9));
		} else
			throw new DRuntimeException("ERRO NO VIA CEP");

		return addressData;
	}

	// -------------------------------------------------------------------------
}
