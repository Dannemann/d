package br.com.dannemann.core.util;

public final class UtilObject {

	public static boolean isTrue(final Object obj) {
		return obj != null && obj.toString().toLowerCase().equals("true");
	}

}
