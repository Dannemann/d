package br.com.dannemann.d.persistence.jpa.test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * All tests must inherit this class.
 */
@ContextConfiguration("classpath*:spring-test.xml")
public abstract class AbstractTest extends AbstractTransactionalJUnit4SpringContextTests {

	public static final String TENANT = "development";

}
