package br.com.dannemann.d.persistence.jpa.test.repository;

import br.com.dannemann.persistence.jpa.repository.CrudRepository;

public interface GenericRepository extends CrudRepository {
}
