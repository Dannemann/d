package br.com.dannemann.d.persistence.jpa.test.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.dannemann.persistence.CrudJpaRepository;

public abstract class AbstractTestRepository<E, P> extends CrudJpaRepository<E, P> {

	@PersistenceContext(unitName = "testEntityManager")
	private EntityManager testEntityManager;

	@Override
	protected EntityManager getPersister(final String tenantID) {
		return testEntityManager;
	}

}
