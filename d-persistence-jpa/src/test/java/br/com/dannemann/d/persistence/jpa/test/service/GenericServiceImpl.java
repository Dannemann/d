package br.com.dannemann.d.persistence.jpa.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dannemann.d.persistence.jpa.test.repository.GenericRepository;
import br.com.dannemann.persistence.CrudService;

@Service
public class GenericServiceImpl extends CrudService implements GenericService {

	@Autowired
	private GenericRepository genericRepository;

	@Override
	protected GenericRepository getRepository() {
		return genericRepository;
	}

}
