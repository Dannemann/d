package br.com.dannemann.d.persistence.jpa.test.tests;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.dannemann.core.util.UtilString;
import br.com.dannemann.d.persistence.jpa.test.AbstractTest;
import br.com.dannemann.d.persistence.jpa.test.model.domain.MatPathModel;
import br.com.dannemann.d.persistence.jpa.test.service.GenericService;
import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.domain.materialized_path.MaterializedPathBean;
import br.com.dannemann.persistence.domain.materialized_path.exception.ChildOfItselfException;
import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.dto.Response;
import br.com.dannemann.persistence.test.EntityFactory;

public class MatPathTest extends AbstractTest {

	// -------------------------------------------------------------------------
	// Fields:

	@Autowired
	private GenericService genericService;

//	@Rule
//	public ExpectedException thrown = ExpectedException.none();

	// -------------------------------------------------------------------------
	// Tests:

	@Test
	public void testParentsOnly() throws Exception {
		final Request req = EntityFactory.mockPersist(TENANT, MatPathModel.class);
		final MatPathModel e = (MatPathModel) req.getObject();
		final MaterializedPath matPath1Ann = e.getClass().getMethod("getMatPath1").getAnnotation(MaterializedPath.class);
		final MaterializedPath matPath2Ann = e.getClass().getMethod("getMatPath2").getAnnotation(MaterializedPath.class);

		genericService.save(req);

		if (e.getId() < 5)
			throw new RuntimeException("Can't do this test with id = 5 or less because I generate values below 5 to add as new node on the test.");

		final byte matPath1AnnPad = matPath1Ann.padding();
		final byte matPath2AnnPad = matPath2Ann.padding();
		final String matPath1PaddedID = UtilString.padLeftWithZeroes(e.getId(), matPath1AnnPad);
		final String matPath2PaddedID = UtilString.padLeftWithZeroes(e.getId(), matPath2AnnPad);

		Assert.assertNotNull(e.getId()); 
		Assert.assertEquals(matPath1PaddedID, e.getMatPath1());
		Assert.assertEquals(matPath2PaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Testing with 1 level.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		// Generating a new mat. path.
		final String newMatPath1 = UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 1)), matPath1AnnPad);
		final String newMatPath2 = UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 1)), matPath2AnnPad);

		e.setMatPath1(newMatPath1);
		e.setMatPath2(newMatPath2);

		genericService.save(req);

		Assert.assertEquals(newMatPath1.concat(matPath1Ann.delimiter()).concat(matPath1PaddedID), e.getMatPath1());
		Assert.assertEquals(newMatPath2.concat(matPath2Ann.delimiter()).concat(matPath2PaddedID), e.getMatPath2());

		// -------------------------------------------------------------------------
		// Testing with 1 level.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		// Generating a new mat. path.
		final String newNewMatPath1 = UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 2)), matPath1AnnPad);
		final String newNewMatPath2 = UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 2)), matPath2AnnPad);

		e.setMatPath1(newNewMatPath1);
		e.setMatPath2(newNewMatPath2);

		genericService.save(req);

		Assert.assertEquals(newNewMatPath1.concat(matPath1Ann.delimiter()).concat(matPath1PaddedID), e.getMatPath1());
		Assert.assertEquals(newNewMatPath2.concat(matPath2Ann.delimiter()).concat(matPath2PaddedID), e.getMatPath2());

		// -------------------------------------------------------------------------
		// Testing with 3 levels.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		// Generating a new mat. path.
		final String newNewNewMatPath1 = 
			UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 3)), matPath1AnnPad) + matPath1Ann.delimiter() + newNewMatPath1 + matPath1Ann.delimiter() + newMatPath1;
		final String newNewNewMatPath2 = 
			UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 3)), matPath2AnnPad) + matPath2Ann.delimiter() + newNewMatPath2 + matPath2Ann.delimiter() + newMatPath2;

		e.setMatPath1(newNewNewMatPath1);
		e.setMatPath2(newNewNewMatPath2);

		genericService.save(req);

		final String newNewNewMatPath1PlusPaddedID = newNewNewMatPath1.concat(matPath1Ann.delimiter()).concat(matPath1PaddedID);
		final String newNewNewMatPath2PlusPaddedID = newNewNewMatPath2.concat(matPath2Ann.delimiter()).concat(matPath2PaddedID);

		Assert.assertEquals(newNewNewMatPath1PlusPaddedID, e.getMatPath1());
		Assert.assertEquals(newNewNewMatPath2PlusPaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Set on same level (e.g. add 10.11.12.13 to 10.11.12).

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		e.setMatPath1(newNewNewMatPath1);
		e.setMatPath2(newNewNewMatPath2);

		genericService.save(req);

		Assert.assertEquals(newNewNewMatPath1PlusPaddedID, e.getMatPath1());
		Assert.assertEquals(newNewNewMatPath2PlusPaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// One node up (e.g. add 10.11.12.13 to 10.11).

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		final String newMatPath1OneNodeUp = UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 3)), matPath1AnnPad) + matPath1Ann.delimiter() + newNewMatPath1;
		final String newMatPath2OneNodeUp = UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 3)), matPath2AnnPad) + matPath2Ann.delimiter() + newNewMatPath2;

		e.setMatPath1(newMatPath1OneNodeUp);
		e.setMatPath2(newMatPath2OneNodeUp);

		genericService.save(req);

		Assert.assertEquals(newMatPath1OneNodeUp.concat(matPath1Ann.delimiter()).concat(matPath1PaddedID), e.getMatPath1());
		Assert.assertEquals(newMatPath2OneNodeUp.concat(matPath2Ann.delimiter()).concat(matPath2PaddedID), e.getMatPath2());

		// -------------------------------------------------------------------------
		// Set as a child of itself.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		// Back up for the next section.
		final String matPath1Backup = e.getMatPath1();
		final String matPath2Backup = e.getMatPath2();

		final String newMatPath1ChildItself = e.getMatPath1() + matPath1Ann.delimiter() +  UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 5)), matPath1AnnPad);
		final String newMatPath2ChildItself = e.getMatPath2() + matPath2Ann.delimiter() +  UtilString.padLeftWithZeroes(String.valueOf((e.getId() - 5)), matPath2AnnPad);

		e.setMatPath1(newMatPath1ChildItself);
		e.setMatPath2(newMatPath2ChildItself);

		final Response responseChildItself = genericService.save(req);

		Assert.assertNotNull(responseChildItself.getIssues());
//		Assert.assertEquals(responseChildItself.getIssues().get(0).getExceptionName(), ChildOfItselfException.class.getSimpleName()); // TODO: THIS.

		// -------------------------------------------------------------------------
		// Set both mat. paths to null after so many updates.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", matPath1Backup);
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", matPath2Backup);

		e.setMatPath1(null);
		e.setMatPath2(null);

		genericService.save(req);

		Assert.assertEquals(matPath1PaddedID, e.getMatPath1());
		Assert.assertEquals(matPath2PaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Saving again the same above.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", matPath1PaddedID);
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", matPath2PaddedID);

		genericService.save(req);

		Assert.assertEquals(matPath1PaddedID, e.getMatPath1());
		Assert.assertEquals(matPath2PaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Testing with 3 levels and with the object id already on the end of the new mat. path.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		e.setMatPath1(newNewNewMatPath1PlusPaddedID);
		e.setMatPath2(newNewNewMatPath2PlusPaddedID);

		genericService.save(req);

		Assert.assertEquals(newNewNewMatPath1PlusPaddedID, e.getMatPath1());
		Assert.assertEquals(newNewNewMatPath2PlusPaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Set mat. path to it's own id.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		e.setMatPath1(matPath1PaddedID);
		e.setMatPath2(matPath2PaddedID);

		genericService.save(req);

		Assert.assertEquals(matPath1PaddedID, e.getMatPath1());
		Assert.assertEquals(matPath2PaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Set both mat. paths to null again.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		e.setMatPath1(null);
		e.setMatPath2(null);

		genericService.save(req);

		Assert.assertEquals(matPath1PaddedID, e.getMatPath1());
		Assert.assertEquals(matPath2PaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Set mat. path to it's own id again.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		e.setMatPath1(matPath1PaddedID);
		e.setMatPath2(matPath2PaddedID);

		genericService.save(req);

		Assert.assertEquals(matPath1PaddedID, e.getMatPath1());
		Assert.assertEquals(matPath2PaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// Testing with 3 levels and with the object id already on the end of the new mat. path again.

		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath1", e.getMatPath1());
		MaterializedPathBean.putDBMatPathsInRequest(req, "matPath2", e.getMatPath2());

		e.setMatPath1(newNewNewMatPath1PlusPaddedID);
		e.setMatPath2(newNewNewMatPath2PlusPaddedID);

		genericService.save(req);

		Assert.assertEquals(newNewNewMatPath1PlusPaddedID, e.getMatPath1());
		Assert.assertEquals(newNewNewMatPath2PlusPaddedID, e.getMatPath2());

		// -------------------------------------------------------------------------
		// 

		
	}

	public void testChildrenUpdates() {
	}

	// TODO: Test exceptions in separate methods.

	// -------------------------------------------------------------------------
}
