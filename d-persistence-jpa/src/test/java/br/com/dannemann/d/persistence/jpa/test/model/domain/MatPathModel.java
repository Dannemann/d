package br.com.dannemann.d.persistence.jpa.test.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.jpa.model.DEntityUid;

@Entity
public class MatPathModel extends DEntityUid {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private Short id;
	private String matPath1;
	private String matPath2;
	private String justAStringField;

	// -------------------------------------------------------------------------
	// Constructors:

	public MatPathModel() {
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	@Override
	@Id
	@SequenceGenerator(name = "matPathModelIDSeq", sequenceName = "matPathModelIDSeq", allocationSize = 1, initialValue = 5)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "matPathModelIDSeq")
	public Short getId() {
		return this.id;
	}

	public void setId(final Short id) {
		this.id = id;
	}

	@MaterializedPath(delimiter = ".", padding = 3)
	@Column(length = 100)
	public String getMatPath1() {
		return matPath1;
	}

	public void setMatPath1(final String matPath1) {
		this.matPath1 = matPath1;
	}

	@MaterializedPath(delimiter = "/", padding = 2)
	@Column(length = 100)
	public String getMatPath2() {
		return matPath2;
	}

	public void setMatPath2(final String matPath2) {
		this.matPath2 = matPath2;
	}

	@Column(length = 40, nullable = false)
	public String getJustAStringField() {
		return justAStringField;
	}

	public void setJustAStringField(final String justAStringField) {
		this.justAStringField = justAStringField;
	}

	// D framework - DEntity overrides:

	@Override
	@Version
	public int getVersion() {
		return version;
	}

	// -------------------------------------------------------------------------
}
