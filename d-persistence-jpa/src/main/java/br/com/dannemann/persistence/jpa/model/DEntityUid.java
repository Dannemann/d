package br.com.dannemann.persistence.jpa.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

@MappedSuperclass
@EntityListeners({ DEntityUid.UIDGeneratorListener.class })
public abstract class DEntityUid extends DEntity {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Static:

	// Fields:

	private static final long serialVersionUID = 1L;

	// Inner classes:

	public static class UIDGeneratorListener {

		@PrePersist
		public void onPrePersist(final DEntityUid uidEntity) {
			uidEntity.populateUUID();
		}

	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Instance:

	// -------------------------------------------------------------------------
	// Fields:

	private String uuid;

	// -------------------------------------------------------------------------
	// Constructors:

	public DEntityUid() {
	}

	// -------------------------------------------------------------------------
	// UUID generation:

	private String populateUUID() {
		if (uuid == null)
			uuid = UUID.randomUUID().toString();

		return uuid;
	}

	// -------------------------------------------------------------------------
	// Object overrides:

	@Override
	public int hashCode() {
		return populateUUID().hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		return (o == this || (o instanceof DEntityUid && populateUUID().equals(((DEntityUid) o).populateUUID())));
	}

	@Override
	public String toString() {
		return new StringBuilder(getClass().getName()).append(" [id=").append(getId()).append(", version=").append(version).append(", uuid=").append(uuid).append("]").toString();
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	// "UUID" and "UID" are Oracle reserved keywords, so, we end up with "uuidz" as column name.
	@Column(name = "uuidz", unique = true, nullable = false, updatable = false, length = 36)
	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	// -------------------------------------------------------------------------
}
