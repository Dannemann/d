package br.com.dannemann.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.dannemann.core.util.UtilString;
import br.com.dannemann.persistence.annotation.MaterializedPath;
import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.exception.DPersistenceException;
import br.com.dannemann.persistence.jpa.repository.CrudRepository;
import br.com.dannemann.persistence.util.UtilEntity;

public abstract class CrudJpaRepository<E, P> extends AbstractCrudRepository<E, P> implements CrudRepository<E, P> {

	// -------------------------------------------------------------------------
	// AbstractCrudRepository overrides:

	// Abstract methods:

	protected abstract EntityManager getPersister(final String tenant);

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// CrudRepository overrides:

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Select:

	// -------------------------------------------------------------------------
	// By ID:
	
	@Override
	public E findById(final Request req) {
		final Class<?> objType = getObjectTypeClass(req);
		if (objType == null)
			throw new DPersistenceException("TODO", "OBJECT TYPE IS NULL"); // TODO: WORK ON THIS.

		final E e = (E) req.getObject();
		
		final UtilEntity ue = new UtilEntity(e);
		if (ue.isSinglePrimaryKey()) {
			final Object id = ue.getPrimarykeyValue();
			if (id == null)
				throw new DPersistenceException("TODO", "ID IS NULL"); // TODO: WORK ON THIS.
			
			return (E) em(req).find(objType, id);
		} else if (ue.isCompositePrimaryKey()) {
			throw new DPersistenceException("TEMP", "NOT IMPLEMENTED FOR JPA YET!"); // TODO: WORK ON THIS.
		} else if (ue.hasNoPrimaryKey()) {
			throw new DPersistenceException("TEMP", "NOT IMPLEMENTED FOR JPA YET!"); // TODO: WORK ON THIS.
		} else
			throw new DPersistenceException("TODO", "ESTE ERRO NAO PODE ACONTECER"); // TODO: WORK ON THIS.
	}
	
	// -------------------------------------------------------------------------
	// By example:

	@Override
	public List<E> findByExample(final Request req) {
		throw new DPersistenceException("TEMP", "NOT IMPLEMENTED FOR JPA YET!"); // TODO: WORK ON THIS.
	}

	// -------------------------------------------------------------------------
	// Find all:

	@Override
	public List<E> findAll(final Request req) {
		final Query selectAllQuery = em(req).createQuery("from ".concat(req.getObjectType()));

		return selectAllQuery.getResultList();
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Insert/Update:

	@Override
	public E save(final Request req) {
		Object e = req.getObject();

		// TODO: TRY TO FIND IF IT IS BRAND NEW ENTITY WITH THE REQUEST OBJECT FIRST. IMPLEMENT IT IF THERE'S NO WAY YET.

		final UtilEntity ue = new UtilEntity(e);

		if (ue.hasPopulatedPrimaryKeys())
			e = em(req).merge(e);
		else
			em(req).persist(e);

		return (E) e;
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Delete:

	@Override
	public void delete(final Request req) {
		em(req).remove(req.getObject());
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Execute:

	/**
	 * Execute a CRUD JPQL statement.
	 */
	@Override
	public Object execute(final Request req) {
		final Object queryObj = req.getQuery();
		if (!(queryObj instanceof String))
			throw new DPersistenceException("TODO", "QUERY PARA EXECUTE EH NULA OU NAO É UMA STRING.");
		
		final String queryStr = (String) queryObj;
		if (queryStr.isEmpty())
			throw new DPersistenceException("TODO", "QUERY PARA EXECUTE EH VAZIA.");
		
		final Class<?> objType = getObjectTypeClass(req);
		final Query query;
		
		if (objType == null)
			query = em(req).createQuery(queryStr);
		else
			query = em(req).createQuery(queryStr, objType);
		
		if (isSelectStatement(queryStr))
			return query.getResultList();
		else
			return query.executeUpdate();
	}

	protected boolean isSelectStatement(final String query) {
		if (query.startsWith("from ") || query.startsWith("select "))
			return true;
		
		return false;
	}
	
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// AbstractCrudRepository overrides:

	@Override
	Object createMatPathChildrenUpdateQuery(final Request req, final String fieldName, final MaterializedPath matPathAnn, final String newMatPath, final String dbMatPath) {
		final String fieldNameAliased = new StringBuilder("d").append(".").append(fieldName).toString();

		final String queryStr = new StringBuilder()
			.append("update ").append(req.getObject().getClass().getSimpleName()).append(" ").append("d").append(" ")
			.append("set ").append(fieldNameAliased).append(" = ")
			.append("replace('").append(fieldName).append("', :curr_db_matpath, :new_matpath) ")
			.append("where ").append(fieldNameAliased).append(" like :curr_db_matpath_like")
			.toString();

		final Query query = em(req).createQuery(queryStr)
			.setParameter("curr_db_matpath", dbMatPath)
			.setParameter("new_matpath", newMatPath)
			.setParameter("curr_db_matpath_like", dbMatPath.concat("%"));

		return query;
	}

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Utilities:

	protected Class<?> getObjectTypeClass(final Request req) {
		Class<?> objType = null;

		// Trying to get from Request.
		try {
			final String objTypeStr = req.getObjectType();

			if (UtilString.isNotEmpty(objTypeStr))
				objType = Class.forName(objTypeStr);
		} catch (final ClassNotFoundException e) {
			// TODO: WORK ON THIS.
			e.printStackTrace();
		}
		
		// Trying to get from the entity itself.
		try {
			if (objType == null) {
				final Object obj = req.getObject();
				
				if (obj != null)
					objType = Class.forName(obj.getClass().getName());
			}
		} catch (final ClassNotFoundException e) {
			// TODO: WORK ON THIS.
			e.printStackTrace();
		}

		// Trying to get from AbstractCrudRepository.
		if (objType == null)
			objType = getEntityType();

		// Trying 
//		if (objType == null) {
//			
//		}
	
		return objType;
	}

	protected EntityManager em(final Request request) {
		return getPersister(request.getTenant());
	}

	// -------------------------------------------------------------------------
}
