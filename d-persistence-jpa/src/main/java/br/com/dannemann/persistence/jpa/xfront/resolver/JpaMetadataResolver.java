package br.com.dannemann.persistence.jpa.xfront.resolver;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import br.com.dannemann.persistence.domain.xfront.dto.FieldDescriptorDto;
import br.com.dannemann.persistence.domain.xfront.resolver.XFrontMetadataResolver;

public class JpaMetadataResolver extends XFrontMetadataResolver {

	//--------------------------------------------------------------------------
	// DAnnotationResolver overrides:

	@Override
	protected void resolvePropertyAnnotations(Method getterMethod, FieldDescriptorDto fieldDescriptorDtoToFill) {
		super.resolvePropertyAnnotations(getterMethod, fieldDescriptorDtoToFill);
		resolveJPAAnnotations(getterMethod, fieldDescriptorDtoToFill);
	}

	//--------------------------------------------------------------------------
	// Private interface:

	private void resolveJPAAnnotations(final Method entityMethod, final FieldDescriptorDto fieldDescriptorDto) {
		// @id
		final Id jpaIdAnn = entityMethod.getAnnotation(Id.class);
		if (jpaIdAnn != null) {
			fieldDescriptorDto.setIsMainID(true);
			fieldDescriptorDto.setIndex(0);
			fieldDescriptorDto.setIsInsertable(false);
		}

		// @Column
		final Column jpaColumnAnn = entityMethod.getAnnotation(Column.class);
		if (jpaColumnAnn != null) {
			fieldDescriptorDto.setLength(jpaColumnAnn.length());
			fieldDescriptorDto.setIsNullable(jpaColumnAnn.nullable());
			fieldDescriptorDto.setIsUnique(jpaColumnAnn.unique());
			fieldDescriptorDto.setIsInsertable(jpaColumnAnn.insertable());
			fieldDescriptorDto.setIsUpdatable(jpaColumnAnn.updatable());
		}

		// @JoinColumn
		final JoinColumn jpaJoinColumnAnnotation = entityMethod.getAnnotation(JoinColumn.class);
		if (jpaJoinColumnAnnotation != null) {
//			fieldDescriptorBean.setIsManyToOne(true); // TODO: TEST @JoinColumn WITHOUT @ManyToOne TO SEE IF IT WORKS AND WHETHER I NEED THIS LINE.
			fieldDescriptorDto.setIsNullable(jpaJoinColumnAnnotation.nullable());
			fieldDescriptorDto.setIsUnique(jpaJoinColumnAnnotation.unique());
			fieldDescriptorDto.setIsInsertable(jpaJoinColumnAnnotation.insertable());
			fieldDescriptorDto.setIsUpdatable(jpaJoinColumnAnnotation.updatable());
		}

		// @ManyToOne
		final ManyToOne jpaManyToOneAnnotation = entityMethod.getAnnotation(ManyToOne.class);
		if (jpaManyToOneAnnotation != null)
			fieldDescriptorDto.setIsManyToOne(true);

		// @OneToMany
		final OneToMany jpaOneToManyAnnotation = entityMethod.getAnnotation(OneToMany.class);
		if (jpaOneToManyAnnotation != null) {
			fieldDescriptorDto.setIsOneToMany(true);
			fieldDescriptorDto.setOneToManyMappedBy(jpaOneToManyAnnotation.mappedBy());

			final Class<?> oneToManyParametrizedType = (Class<?>) (((ParameterizedType) entityMethod.getGenericReturnType()).getActualTypeArguments()[0]);
			fieldDescriptorDto.setOneToManyTarget(oneToManyParametrizedType.getName());
		}

		// @ManyToMany
//		final ManyToMany jpaManyToManyAnnotation = entityMethod.getAnnotation(ManyToMany.class);
//		final JoinTable jpaJoinTableAnnotation = entityMethod.getAnnotation(JoinTable.class);
		/*else if (jpaManyToManyAnnotation != null) {
			fieldDescriptorBean.setIsManyToMany(true);
			fieldDescriptorBean.setIsNullable(false);
			fieldDescriptorBean.setIsUpdatable(true);
			fieldDescriptorBean.setIsUnique(true);
			fieldDescriptorBean.setManyToManyTarget(fieldDescriptorAnnotation.manyToManyTarget());
			fieldDescriptorBean.setManyToManyTableName(jpaJoinTableAnnotation.name());
			fieldDescriptorBean.setManyToManyJoinColumnName(jpaJoinTableAnnotation.joinColumns()[0].name());
			fieldDescriptorBean.setManyToManyInverseJoinColumnName(jpaJoinTableAnnotation.inverseJoinColumns()[0].name());
			include = true;
		}*/

		// @Version
		final Version jpaVersionAnnotation = entityMethod.getAnnotation(Version.class);
		if (jpaVersionAnnotation != null) {
			fieldDescriptorDto.setIndex(-1);
			fieldDescriptorDto.setIsVersionField(true);
		}
	}

	//--------------------------------------------------------------------------
}
