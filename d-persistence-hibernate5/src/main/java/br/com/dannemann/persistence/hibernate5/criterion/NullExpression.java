package br.com.dannemann.persistence.hibernate5.criterion;

public class NullExpression extends org.hibernate.criterion.NullExpression {

	protected NullExpression(final String propertyName) {
		super(propertyName);
	}

}
