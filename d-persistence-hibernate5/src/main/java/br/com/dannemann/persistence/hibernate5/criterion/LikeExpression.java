// Copied from Hibernate 5.2.10.Final.

package br.com.dannemann.persistence.hibernate5.criterion;

import java.util.Locale;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.TypedValue;

/**
 * A criterion representing a "like" expression
 *
 * @author Scott Marlow
 * @author Steve Ebersole
 */
public class LikeExpression implements Criterion {
	private final String propertyName;
	private final Object value;
	private final Character escapeChar;
	private final boolean ignoreCase;

// D framework features BEGIN

	// Diacriticless search: https://forum.hibernate.org/viewtopic.php?f=1&t=970231

	private boolean diacriticlessSearch;

	protected LikeExpression(final String propertyName, final String value, final MatchMode matchMode, final Character escapeChar, final boolean ignoreCase, boolean diacriticlessSearch) {
		this(propertyName, matchMode.toMatchString(value), escapeChar, ignoreCase);

		this.diacriticlessSearch = diacriticlessSearch;
	}

// END

	protected LikeExpression(
			String propertyName,
			String value,
			Character escapeChar,
			boolean ignoreCase) {
		this.propertyName = propertyName;
		this.value = value;
		this.escapeChar = escapeChar;
		this.ignoreCase = ignoreCase;
	}

	protected LikeExpression(String propertyName, String value) {
		this( propertyName, value, null, false );
	}

	@SuppressWarnings("UnusedDeclaration")
	protected LikeExpression(String propertyName, String value, MatchMode matchMode) {
		this( propertyName, matchMode.toMatchString( value ) );
	}

	protected LikeExpression(
			String propertyName,
			String value,
			MatchMode matchMode,
			Character escapeChar,
			boolean ignoreCase) {
		this( propertyName, matchMode.toMatchString( value ), escapeChar, ignoreCase );
	}

// D framework features BEGIN

	// Original method.
//	@Override
//	public String toSqlString(Criteria criteria,CriteriaQuery criteriaQuery) {
//		final Dialect dialect = criteriaQuery.getFactory().getDialect();
//		final String[] columns = criteriaQuery.findColumns( propertyName, criteria );
//		if ( columns.length != 1 ) {
//			throw new HibernateException( "Like may only be used with single-column properties" );
//		}
//
//		final String escape = escapeChar == null ? "" : " escape \'" + escapeChar + "\'";
//		final String column = columns[0];
//		if ( ignoreCase ) {
//			if ( dialect.supportsCaseInsensitiveLike() ) {
//				return column +" " + dialect.getCaseInsensitiveLike() + " ?" + escape;
//			}
//			else {
//				return dialect.getLowercaseFunction() + '(' + column + ')' + " like ?" + escape;
//			}
//		}
//		else {
//			return column + " like ?" + escape;
//		}
//	}

	@Override
	public String toSqlString(final Criteria criteria, final CriteriaQuery criteriaQuery) {
		final Dialect dialect = criteriaQuery.getFactory().getDialect();
		final String[] columns = criteriaQuery.findColumns(propertyName, criteria);

		if (columns.length != 1)
			throw new HibernateException("Like may only be used with single-column properties");

		final String escape = escapeChar == null ? "" : " escape \'" + escapeChar + "\'";
		final String column = columns[0];

		if (diacriticlessSearch) {
			if (ignoreCase) {
				if (dialect.supportsCaseInsensitiveLike())
					return "translate("+column+", "+AssociationExample.TRANSLATE+") "+dialect.getCaseInsensitiveLike()+" translate(?, "+AssociationExample.TRANSLATE+")"+escape;
				else
					return "translate("+dialect.getLowercaseFunction()+'('+column+"), "+AssociationExample.TRANSLATE+") like translate(?, "+AssociationExample.TRANSLATE+")"+escape;
			} else
				return "translate("+column+", "+AssociationExample.TRANSLATE+") like translate(?, "+AssociationExample.TRANSLATE+")"+escape;
		} else {
			if (ignoreCase) {
				if (dialect.supportsCaseInsensitiveLike())
					return column + " " + dialect.getCaseInsensitiveLike() + " ?" + escape;
				else
					return dialect.getLowercaseFunction() + '(' + column + ')' + " like ?" + escape;
			} else
				return column + " like ?" + escape;
		}
	}

// END

	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) {
		final String matchValue = ignoreCase ? value.toString().toLowerCase(Locale.ROOT) : value.toString();

		return new TypedValue[] { criteriaQuery.getTypedValue( criteria, propertyName, matchValue ) };
	}
}
