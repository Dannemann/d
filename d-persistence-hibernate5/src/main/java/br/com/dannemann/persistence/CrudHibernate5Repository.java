package br.com.dannemann.persistence;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

import br.com.dannemann.persistence.CrudJpaRepository;
import br.com.dannemann.persistence.dto.Request;
import br.com.dannemann.persistence.hibernate5.criterion.AssociationExample;
import br.com.dannemann.persistence.hibernate5.repository.CrudRepository;

public abstract class CrudHibernate5Repository<E, P> extends CrudJpaRepository<E, P> implements CrudRepository<E, P> {

	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// CrudJpaRepository overrides:

	// -------------------------------------------------------------------------
	// Abstract methods:

	protected abstract Session getPersister(final String tenant);

	// -------------------------------------------------------------------------
	// Select:

	@Override
	public List<E> findByExample(final Request req) {
		final Object entity = req.getObject();

		final AssociationExample assocExample = AssociationExample.create(entity)
			.enableLike()
			.diacriticlessSearch()
			.ignoreCase();

		final Criteria criteria = em(req).createCriteria(entity.getClass()).add(assocExample);
		final List<E> resultList = criteria.list();

		return resultList;
	}

	// -------------------------------------------------------------------------
	// Utilities:

	protected Session em(final Request request) {
		return getPersister(request.getTenant());
	}

	// -------------------------------------------------------------------------
}
