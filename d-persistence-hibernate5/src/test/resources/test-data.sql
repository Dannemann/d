-- PhoneNumberProvider:

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (1, '4c9595ea-429b-4d9c-bced-64412132b366', '41', 'TIM', 1);

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (2, '1b1d5485-56ba-40c1-b2ae-3a33b0a2493b', '21', 'Claro', 1);

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (3, 'b90d5ad4-2e02-429b-8061-2f6c9e84e271', '12', 'Algar', 1);

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (4, 'c566ac77-088b-4977-9a20-6b453e5d2f8c', '31', 'Oi', 1);

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (5, 'e268f709-762a-4efc-84ec-a3ec082d9113', '15', 'Vivo', 1);

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (6, 'c111b73c-9f39-4dac-9bf5-e224bb52a494', '43', 'Sercomtel', 1);

INSERT INTO d.phonenumberprovider(id, uuidz, code, name, version)
VALUES (7, '1ccf6ccb-f903-40b0-b294-e7f84208af77', NULL, 'NEXTEL', 1);

-- PersonType:

INSERT INTO d.persontype(id, uuidz, description, version)
VALUES (1, '260751f0-edf3-44fd-b567-c1d2f36ce4f0', 'Colaborador', 1);

INSERT INTO d.persontype(id, uuidz, description, version)
VALUES (2, '1442f7fb-1a77-44a9-b4d5-7d0c3c5df85e', 'Contractor', 1);

-- Person:

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (1, 'c9b12bba-d443-4d39-8b2c-7484f020399e', TRUE, '19841125', TRUE, 'Jean Dannemann Carone', 1, NULL, 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (2, 'cf7529d8-e449-425e-af9b-a2b03c652cc9', TRUE, '20010325', FALSE, 'Marília Gabriela Tavares', 1, 'Ligar somente após as 5 e meia.', 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (3, '8508bd37-f675-431b-9fa6-3ee639aec3db', TRUE, '19650122', TRUE, 'Murilo Caixeta Soares Lima', 1, 'Mudou. Endereço, telefone e etc não são mais dele.', 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (4, '312fc00c-b6ab-41e4-96e8-4bccb8583355', TRUE, '20000306', TRUE, 'Márcio Santos Cenevila', 1, NULL, 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (5, '4a27e546-a7ec-43be-bddf-cdcfe3616bbd', TRUE, '19871224', FALSE, 'Alessandra Tavares Silva', 1, NULL, 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (6, '79d7839d-4110-4848-a3c0-2399b42ea76a', TRUE, '19880214', TRUE, 'Marcos Pereira Valério', 1, NULL, 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (7, 'dacf4e14-1e6e-47a7-b860-8b23c0d1af0f', TRUE, '19991124', FALSE, 'Rosana Xavier', 1, 'Está hospitalizada. Procurar informações antes de cobrar.', 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (8, '1025cc51-a991-4e15-b093-6db46094d641', FALSE, '19660129', TRUE, 'Marcelino Souza Silva', 1, NULL, 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (9, '14998a20-b0f1-45b5-84d5-3cc3596619c9', FALSE, '19970615', TRUE, 'Marcone Da Silva Júnior', 1, 'Confirmou que não vai pagar. Levar o caso para o Rogério.', 1);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (10, '07e79634-83d2-4dcd-a3fc-9e40f9ba6701', FALSE, '19850726', FALSE, 'Alessandra Hondjakhof Pacheco', 1, NULL, 2);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (11, '9c58aad2-c0c5-4255-bd1b-d11bf31f423c', TRUE, '19780722', TRUE, 'Marcos Moreira Matos', 1, NULL, 2);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (12, '1daf0eab-4163-4c61-a0f7-12402e3bc1f2', TRUE, '19780716', TRUE, 'Marcelo Alcântara Junior', 1, NULL, 2);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (13, '9592bdc2-e2a5-4213-b2e7-ab0a22654101', TRUE, '19780701', TRUE, 'Alêssan De Matos Cabral', 1, NULL, 2);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (14, 'b2535e09-6bd0-41a5-a908-9d8b814cffe9', TRUE, '19790608', FALSE, 'Bruna De Pádoa Vicchi', 1, 'Ligar antes das 3 da tarde.', 2);

INSERT INTO d.person(id, uuidz, active, birthdate, gender, name, version, details, persontype_id)
VALUES (15, 'e05ff5fa-d782-4baf-bc2a-94d8119a38e6', TRUE, '19951015', FALSE, 'Marcia Da Silva Cavalcanti', 1, NULL, 2);

-- PersonPhoneNumber:

INSERT INTO d.personphonenumber(id, uuidz, areacode, countrycode, description, "number", version, provider_id, person_id)
VALUES (1, '4e43523c-a48a-4e98-ac97-5502988f8df7', '34', '55', NULL, '999988856', 1, 3, 5);

INSERT INTO d.personphonenumber(id, uuidz, areacode, countrycode, description, "number", version, provider_id, person_id)
VALUES (2, '20beb427-651c-405e-aa7b-891eb5320a50', '21', '55', NULL, '991488638', 1, 3, 13);

INSERT INTO d.personphonenumber(id, uuidz, areacode, countrycode, description, "number", version, provider_id, person_id)
VALUES (3, '61f4df4f-ed6f-4e1b-9b20-6a9e722f7762', '34', '55', NULL, '998566352', 1, 3, 13);

INSERT INTO d.personphonenumber(id, uuidz, areacode, countrycode, description, "number", version, provider_id, person_id)
VALUES (4, '689a4279-8671-4c0d-9518-728a7ee4b69d', '34', '55', 'Melhor telefone para achá-lo.', '978855425', 1, 1, 5);

INSERT INTO d.personphonenumber(id, uuidz, areacode, countrycode, description, "number", version, provider_id, person_id)
VALUES (5, '06a83218-cdc3-4b3f-b1eb-734ef4fbf72a', '21', '55', NULL, '988566632', 1, 2, 1);

INSERT INTO d.personphonenumber(id, uuidz, areacode, countrycode, description, "number", version, provider_id, person_id)
VALUES (6, '454fa7ac-454c-46be-a725-c4155a322e95', '27', '55', 'Casa do irmão.', '38222695', 1, 1, 13);



-- ecf3f098-fbce-47c9-8c55-0c614095a56f
-- 734e4f6d-ed16-4f80-a736-206340088dab
-- fe250c3f-6310-4bc7-8d9b-f8d311e4dcb5
-- b2cfe583-4b76-40ed-b26b-d3398496cc94
-- 0f1b2e9f-bd6c-4034-8cf7-59b4d66bcc0c
-- b1d30384-8c88-47ee-b58e-0b431c4ca00d
-- 5c98254c-378d-414e-a047-80098b00feb8
-- ff7ac877-8488-4318-b353-8a6e43177eda
-- 47ef8ad1-7621-4d74-ad16-dd50f79c3835
-- 56ef2065-26ae-4c61-9c01-aa2873f96cf0
-- 767edc9d-813d-4f52-afc4-5a0560665819
-- dbfa498d-8692-4829-8a4e-baa467ce56fb
-- c27a57ed-6959-42fa-8329-4db99b64472b
-- 8418080d-ee7d-4983-adbe-df6d3fab07d3
-- eedb3ab0-712b-451d-946a-9a0e2d506113
-- d264b960-38fc-45fb-b67f-8ea845c8d2e4
