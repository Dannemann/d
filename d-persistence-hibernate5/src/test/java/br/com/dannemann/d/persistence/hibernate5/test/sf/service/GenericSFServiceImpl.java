package br.com.dannemann.d.persistence.hibernate5.test.sf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dannemann.d.persistence.hibernate5.test.sf.repository.GenericSFRepository;
import br.com.dannemann.persistence.CrudService;

@Service
public class GenericSFServiceImpl extends CrudService implements GenericSFService {

	@Autowired
	private GenericSFRepository genericRepository;

	@Override
	protected GenericSFRepository getRepository() {
		return genericRepository;
	}

}
