package br.com.dannemann.d.persistence.hibernate5.test.sf.repository;

import br.com.dannemann.persistence.hibernate5.repository.CrudRepository;

public interface GenericSFRepository extends CrudRepository {
}
