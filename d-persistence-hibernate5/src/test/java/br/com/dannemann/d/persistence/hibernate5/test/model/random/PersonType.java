package br.com.dannemann.d.persistence.hibernate5.test.model.random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

import br.com.dannemann.persistence.jpa.model.DEntityUid;

@Entity
public class PersonType extends DEntityUid {

	// --------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private Short id;

	private String description;

	// --------------------------------------------------------------------------
	// Constructors:

	public PersonType() {
	}

	// --------------------------------------------------------------------------
	// Getters and setters:

	@Override
	@Id
	@GenericGenerator(name = "personTypeIDSeq", strategy = "sequence")
	@GeneratedValue(generator = "personTypeIDSeq")
	public Short getId() {
		return id;
	}

	public void setId(final Short id) {
		this.id = id;
	}

	@Column(length = 30, nullable = false, unique = true)
	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	// DEntity overrides:

	@Override
	@Version
	public int getVersion() {
		return version;
	}

	// --------------------------------------------------------------------------
}
