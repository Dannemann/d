package br.com.dannemann.d.persistence.hibernate5.test.em.repository;

import br.com.dannemann.persistence.hibernate5.repository.CrudRepository;

public interface GenericEMRepository extends CrudRepository {
}
