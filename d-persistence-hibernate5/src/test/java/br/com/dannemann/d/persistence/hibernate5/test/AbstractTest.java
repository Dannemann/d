package br.com.dannemann.d.persistence.hibernate5.test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * Do NOT extend this class for testing. Extend it's sub-classes instead.
 */
@ContextConfiguration("classpath*:spring-test.xml")
@Sql("classpath:test-data.sql")
public abstract class AbstractTest extends AbstractTransactionalJUnit4SpringContextTests {

	public static final String TENANT = "development";

}
