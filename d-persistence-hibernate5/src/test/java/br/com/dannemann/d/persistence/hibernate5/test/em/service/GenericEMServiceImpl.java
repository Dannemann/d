package br.com.dannemann.d.persistence.hibernate5.test.em.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dannemann.d.persistence.hibernate5.test.em.repository.GenericEMRepository;
import br.com.dannemann.persistence.CrudService;

@Service
public class GenericEMServiceImpl extends CrudService implements GenericEMService {

	@Autowired
	private GenericEMRepository genericRepository;

	@Override
	protected GenericEMRepository getRepository() {
		return genericRepository;
	}

}
