package br.com.dannemann.d.persistence.hibernate5.test.tests;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.dannemann.d.persistence.hibernate5.test.AbstractTestSessionFactory;
import br.com.dannemann.d.persistence.hibernate5.test.model.random.Person;
import br.com.dannemann.d.persistence.hibernate5.test.model.random.PersonType;
import br.com.dannemann.d.persistence.hibernate5.test.sf.service.GenericSFService;
import br.com.dannemann.persistence.dto.Request;

public class DRepositorySFTest extends AbstractTestSessionFactory {

	// -------------------------------------------------------------------------
	// Fields:

	@Autowired
	private GenericSFService genericService;

	// -------------------------------------------------------------------------
	// Tests:

	@Test
	public void testSelectByExample() {
		final PersonType personType = new PersonType();
		personType.setId((short) 2);

		final Person person1 = new Person();
		person1.setPersonType(personType);
		person1.setName("aLêss");
		person1.setGender(false);

		final Request request = new Request();
		request.setObject(person1);

		genericService.save(request);

		System.out.println(" $$$$$$$$ %%%%%%%%%%%" + genericService);
	}

	// -------------------------------------------------------------------------
}
