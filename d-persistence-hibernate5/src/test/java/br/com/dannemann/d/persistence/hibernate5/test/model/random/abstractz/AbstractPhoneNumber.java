package br.com.dannemann.d.persistence.hibernate5.test.model.random.abstractz;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import br.com.dannemann.d.persistence.hibernate5.test.model.random.PhoneNumberProvider;
import br.com.dannemann.persistence.jpa.model.DEntityUid;

@MappedSuperclass
public abstract class AbstractPhoneNumber extends DEntityUid {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private PhoneNumberProvider provider;
	private String countryCode;
	private String areaCode;
	private String number;
	private String description;

	// -------------------------------------------------------------------------
	// Constructors:

	public AbstractPhoneNumber() {
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	@ManyToOne(fetch = FetchType.LAZY)
	public PhoneNumberProvider getProvider() {
		return provider;
	}

	public void setProvider(final PhoneNumberProvider provider) {
		this.provider = provider;
	}

	@Column(length = 7)
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
	}

	@Column(length = 7)
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(final String areaCode) {
		this.areaCode = areaCode;
	}

	@Column(length = 20, nullable = false)
	public String getNumber() {
		return number;
	}

	public void setNumber(final String number) {
		this.number = number;
	}

	@Column(length = 100)
	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	// DEntity overrides:

	@Override
	@Version
	public int getVersion() {
		return version;
	}

	// -------------------------------------------------------------------------
}
