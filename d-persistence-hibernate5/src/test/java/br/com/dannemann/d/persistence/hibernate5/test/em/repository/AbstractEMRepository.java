package br.com.dannemann.d.persistence.hibernate5.test.em.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

import br.com.dannemann.persistence.CrudHibernate5Repository;

public abstract class AbstractEMRepository<E, P> extends CrudHibernate5Repository<E, P> {

	@PersistenceContext(unitName = "testEntityManager")
	private EntityManager testEntityManager;

	@Override
	protected Session getPersister(final String tenant) {
		return testEntityManager.unwrap(Session.class);
	}

}
