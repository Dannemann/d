package br.com.dannemann.d.persistence.hibernate5.test.model.random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

import br.com.dannemann.persistence.jpa.model.DEntityUid;

@Entity
public class PhoneNumberProvider extends DEntityUid {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private Short id;

	private String name;
	private String code;

	// -------------------------------------------------------------------------
	// Constructors:

	public PhoneNumberProvider() {
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	@Override
	@Id
	@SequenceGenerator(name = "phoneNumberOperatorIDSeq", sequenceName = "phoneNumberOperatorIDSeq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phoneNumberOperatorIDSeq")
	public Short getId() {
		return this.id;
	}

	public void setId(final Short id) {
		this.id = id;
	}

	@Column(length = 30, nullable = false, unique = true)
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Column(length = 15)
	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	// DEntity overrides:

	@Override
	@Version
	public int getVersion() {
		return version;
	}

	// -------------------------------------------------------------------------
}
