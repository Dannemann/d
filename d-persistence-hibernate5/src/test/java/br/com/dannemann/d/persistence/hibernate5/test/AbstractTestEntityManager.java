package br.com.dannemann.d.persistence.hibernate5.test;

import org.springframework.test.context.ContextConfiguration;

/**
 * All object manager related tests must inherit this class.
 */
@ContextConfiguration("classpath*:spring-test-object-manager.xml")
public abstract class AbstractTestEntityManager extends AbstractTest {
}
