package br.com.dannemann.d.persistence.hibernate5.test.sf.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.dannemann.persistence.CrudHibernate5Repository;

public abstract class AbstractSFRepository<E, P> extends CrudHibernate5Repository<E, P> {

	@Autowired
	private SessionFactory testSessionFactory;

	@Override
	protected Session getPersister(final String tenant) {
		return testSessionFactory.getCurrentSession();
	}

}
