package br.com.dannemann.d.persistence.hibernate5.test.model.random;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.dannemann.d.persistence.hibernate5.test.model.random.abstractz.AbstractPhoneNumber;

@Entity
public class PersonPhoneNumber extends AbstractPhoneNumber {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private Short id;

	private Person person;

	// -------------------------------------------------------------------------
	// Constructors:

	public PersonPhoneNumber() {
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	@Override
	@Id
	@SequenceGenerator(name = "personPhoneNumberIDSeq", sequenceName = "personPhoneNumberIDSeq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personPhoneNumberIDSeq")
	public Short getId() {
		return this.id;
	}

	public void setId(final Short id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	public Person getPerson() {
		return person;
	}

	public void setPerson(final Person person) {
		this.person = person;
	}

	// -------------------------------------------------------------------------
}
