package br.com.dannemann.d.persistence.hibernate5.test.model.random;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

import br.com.dannemann.persistence.jpa.model.DEntityUid;

@Entity
public class Person extends DEntityUid {

	// -------------------------------------------------------------------------
	// Fields:

	private static final long serialVersionUID = 1L;

	private Short id;

	private PersonType personType;
	private String name;
	private String birthdate;
	private Boolean gender;
	private Boolean active;
	private String details;

	private List<PersonPhoneNumber> personPhoneNumbers;
//	private List<Person> personContacts; // TODO: IMPLEMENT THIS TEST!

	// -------------------------------------------------------------------------
	// Constructors:

	public Person() {
	}

	// -------------------------------------------------------------------------
	// Getters and setters:

	@Override
	@Id
	@GenericGenerator(name = "personIDSeq", strategy = "sequence")
	@GeneratedValue(generator = "personIDSeq")
	public Short getId() {
		return this.id;
	}

	public void setId(final Short id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	public PersonType getPersonType() {
		return personType;
	}

	public void setPersonType(final PersonType personType) {
		this.personType = personType;
	}

	@Column(length = 100, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Column(length = 8)
	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(final String birthdate) {
		this.birthdate = birthdate;
	}

	@Column
	public Boolean getGender() {
		return gender;
	}

	public void setGender(final Boolean gender) {
		this.gender = gender;
	}

	@Column
	public Boolean getActive() {
		return active;
	}

	public void setActive(final Boolean active) {
		this.active = active;
	}

	@Column(length = 500)
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@OneToMany(mappedBy = "person")
	public List<PersonPhoneNumber> getPersonPhoneNumbers() {
		return personPhoneNumbers;
	}

	public void setPersonPhoneNumbers(final List<PersonPhoneNumber> personPhoneNumbers) {
		this.personPhoneNumbers = personPhoneNumbers;
	}

	// DEntity overrides:

	@Override
	@Version
	public int getVersion() {
		return version;
	}

	// -------------------------------------------------------------------------
}
