package br.com.dannemann.d.persistence.hibernate5.test;

import org.springframework.test.context.ContextConfiguration;

/**
 * All session factory related tests must inherit this class.
 */
@ContextConfiguration("classpath*:spring-test-session-factory.xml")
public abstract class AbstractTestSessionFactory extends AbstractTest {
}
