package br.com.dannemann.xfront;

import br.com.dannemann.core.domain.CoreRegistryUpdateException;
import br.com.dannemann.core.log.DLogger;
import br.com.dannemann.persistence.domain.xfront.resolver.XFrontMetadataResolver;
import br.com.dannemann.xfront.domain.DProperties;
import br.com.dannemann.xfront.domain.XFrontCoreRegistry;

// TODO: i18n, exception
public final class XFrontInitializer {

	private static final DLogger LOGGER = new DLogger(XFrontInitializer.class);

	public static void initialize(
		final String appName,
		final String appId,
		final XFrontMetadataResolver xFrontMetadataResolver,
		final String[] entityPackages,
		final String appPathOnServer)
	{
		LOGGER.info("");
		LOGGER.info(new StringBuilder("initialize(\"")
			.append(appName).append("\", \"")
			.append(appId).append("\", \"")
			.append(xFrontMetadataResolver).append("\", \"")
			.append(entityPackages).append("\", \"")
			.append(appPathOnServer).append("\");")
			.toString());

		try {
			XFrontCoreRegistry.addApplication(appId);
			XFrontCoreRegistry.setAppPathOnServer(appId, appPathOnServer);
			XFrontCoreRegistry.loadEntities(appId, entityPackages);
			XFrontCoreRegistry.resolveEntitiesMetadata(appId, xFrontMetadataResolver);
		} catch (final CoreRegistryUpdateException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Initializes Dannemann framework assuming that "d.properties" file is in the project's classpath. 
	 */
	public static void initialize(final XFrontMetadataResolver xFrontMetadataResolver) {
		LOGGER.info("Initializing with \"d.properties\" configuration.");

		initialize(
			DProperties.getAppName(),
			DProperties.getAppId(),
			xFrontMetadataResolver,
			DProperties.getEntityPackages().split(","),
			DProperties.getAppPathOnServer());
	}

}
