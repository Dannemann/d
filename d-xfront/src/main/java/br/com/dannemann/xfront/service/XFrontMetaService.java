package br.com.dannemann.xfront.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.dannemann.core.domain.CoreRegistry;
import br.com.dannemann.core.domain.CoreRegistryUpdateException;
import br.com.dannemann.core.exception.DRuntimeException;
import br.com.dannemann.core.util.DUtilDate;
import br.com.dannemann.core.util.DUtilEncryption;
import br.com.dannemann.core.util.DUtilFile;
import br.com.dannemann.core.util.DUtilString;

public class XFrontMetaService {

	public Map<String, Object> getRegistry(final String application) {
		try {
			return CoreRegistry.getApplicationRegistry(application);
		} catch (final CoreRegistryUpdateException e) {
			throw new RuntimeException(e);
		}
	}

	public Map<String, String> now() {
		return DUtilDate.now();
	}

//	public String[] getAllDeployedDataSources() throws Exception {
//		return new DUtilJBoss().getAllDeployedDataSources(((IServiceLocator) getServiceLocatorInstance()).getContext());
//	}

//	public Map<String, String> searchCEPOnWebService(final String cep) throws Exception {
//		final DUtilCEPBean dUtilCEPBean = DUtilCEPBean.searchCep(cep);
//
//		if (dUtilCEPBean.getException() != null)
//			throw dUtilCEPBean.getException();
//
//		final Map<String, String> returnMap = new HashMap<String, String>();
//		returnMap.put("neighborhood", dUtilCEPBean.getBairro());
//		returnMap.put("cep", dUtilCEPBean.getCep());
//		returnMap.put("city", dUtilCEPBean.getCidade());
//		returnMap.put("street", dUtilCEPBean.getLogradouro());
//		returnMap.put("streetTypePlusStreet", dUtilCEPBean.getLogradouroFull());
//		returnMap.put("streetType", dUtilCEPBean.getLogradouroType());
//		returnMap.put("resultText", dUtilCEPBean.getResultText());
//		returnMap.put("uf", dUtilCEPBean.getUf());
//		returnMap.put("resultCode", String.valueOf(dUtilCEPBean.getResulCode()));
//
//		return returnMap;
//	}

	public String encrypt(final String plaintext) {
		return DUtilEncryption.encrypt(plaintext);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	public String[] getOnEnterpriseHomeFolder(final Map<String, Object> parameters) throws Exception {
		final Object appPathOnServer = parameters.get("app.path.on.server");
		final Object enterprise = parameters.get("enterprise");
		final Object folder = parameters.get("folder");
		final Object file = parameters.get("file");

		if (!DUtilString.hasValue(appPathOnServer))
//			throw new DRuntimeException("${100104}", DRuntimeException.ERROR, DRuntimeException.ORIGIN_DFXGUI);
			throw new DRuntimeException("TODO","${100104}");
		if (!DUtilString.hasValue(enterprise))
//			throw new DRuntimeException("${100105}", DRuntimeException.ERROR, DRuntimeException.ORIGIN_DFXGUI);
			throw new DRuntimeException("TODO","${100105}");
		if (!DUtilString.hasValue(folder))
//			throw new DRuntimeException("${100106}", DRuntimeException.ERROR, DRuntimeException.ORIGIN_DFXGUI);
			throw new DRuntimeException("TODO","${100106}");

		final File targetFolder = new File((new StringBuilder()).append(appPathOnServer).append("/enterprise/").append(enterprise).append("/").append(folder).toString());

		if (!targetFolder.exists())
			throw new DRuntimeException("TODO"," ### XFrontMetaService.getOnEnterpriseHomeFolder(Map): O caminho dos arquivos a serem obtidos n�o existe.");
		if (!targetFolder.isDirectory())
			throw new DRuntimeException("TODO"," ### XFrontMetaService.getOnEnterpriseHomeFolder(Map): O caminho dos arquivos a serem obtidos n�o � uma pasta.");

		final String[] fileList = targetFolder.list();

		if (DUtilString.hasValue(file)) {
			for (final String fileString : fileList)
				if (fileString.equals(file))
					return new String[] { fileString };

			return null;
		} else
			return fileList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> deleteOnEnterpriseHomeFolder(final Map<String, Object> parameters) throws Exception {
		final Object appPathOnServer = parameters.get("appPathOnServer");
		final Object enterprise = parameters.get("enterprise");
		final Object folder = parameters.get("folder");
		final Object files = parameters.get("files");

		if (!DUtilString.hasValue(appPathOnServer))
			throw new DRuntimeException("TODO","${100104}");
		if (!DUtilString.hasValue(enterprise))
			throw new DRuntimeException("TODO","${100105}");
		if (!DUtilString.hasValue(folder))
			throw new DRuntimeException("TODO","${100106}");

		final String folderPath = (new StringBuilder())
			.append(appPathOnServer).append("/enterprise/").append(enterprise).append("/").append(DUtilFile.removeEndSlash(folder.toString().trim())).toString();
		final File targetFolder = new File(folderPath);

		if (!targetFolder.exists()) {
			if (files == null)
				throw new DRuntimeException("TODO","${100101}");
			else
				throw new DRuntimeException("TODO","${100102}");
		}

		if (!targetFolder.isDirectory())
			throw new DRuntimeException("TODO","${100103}");

		// Deleting files...

		final Map<String, Object> returnObj = new HashMap<String, Object>();
		ArrayList<String> deleted = null;
		ArrayList<String> notDeleted = null;
		ArrayList<String> notExists = null;

		if (files == null) { // No files. Delete the entire folder.
			if (targetFolder.exists()) {
				if (targetFolder.delete()) {
					if (deleted == null)
						deleted = new ArrayList<String>();

					deleted.add(folderPath);
				} else {
					if (notDeleted == null)
						notDeleted = new ArrayList<String>();

					notDeleted.add(folderPath);
				}
			} else {
				if (notExists == null)
					notExists = new ArrayList<String>();

				notExists.add(folderPath);
			}

			returnObj.put("target", "folder");
		} else {
			if (files instanceof String) {
				if (files.toString().equalsIgnoreCase("all")) {
					// TODO: Delete all files inside the specified folder, but do not delete the folder itself.

					returnObj.put("target", "allFiles");
				} else {
					// TODO: Delete only one file.

					returnObj.put("target", "file");
				}
			} else {
				final ArrayList<String> filesArr = (ArrayList<String>) files;

				if (filesArr != null) { // Delete the chosen files.
					for (final String file : filesArr) {
						final File fileToExclude = new File((new StringBuilder()).append(DUtilFile.addEndSlash(folderPath)).append(file).toString());

						if (fileToExclude.exists()) {
							if (fileToExclude.delete()) {
								if (deleted == null)
									deleted = new ArrayList<String>();

								deleted.add(file);
							} else {
								if (notDeleted == null)
									notDeleted = new ArrayList<String>();

								notDeleted.add(file);
							}
						} else {
							if (notExists == null)
								notExists = new ArrayList<String>();

							notExists.add(file);
						}
					}

					returnObj.put("target", "files");
				} else
					throw new DRuntimeException("TODO","${100107}");
			}
		}

		returnObj.put("notExists", notExists);
		returnObj.put("deleted", deleted);
		returnObj.put("notDeleted", notDeleted);
		return returnObj;
	}
	//--------------------------------------------------------------------------
}
