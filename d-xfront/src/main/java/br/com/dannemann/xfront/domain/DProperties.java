package br.com.dannemann.xfront.domain;

public class DProperties extends br.com.dannemann.core.domain.DProperties {

	// TODO: I think I can move app.name, app.id and app.path.on.server to the core module. Need to analyze this.
	//		 Currently, only X-Front utilize these properties.

	public static final String PROP_APP_NAME = "app.name";
	public static final String PROP_APP_ID = "app.id";
	public static final String PROP_ENTITY_PACKAGES = "entity.packages";
	public static final String PROP_APP_PATH_ON_SERVER = "app.path.on.server";
	public static final String PROP_DATABASE = "database";

	public static String getAppName() {
		return getInstance().getProperty(PROP_APP_NAME);
	}

	public static String getAppId() {
		return getInstance().getProperty(PROP_APP_ID);
	}

	public static String getEntityPackages() {
		return getInstance().getProperty(PROP_ENTITY_PACKAGES);
	}

	public static String getAppPathOnServer() {
		return getInstance().getProperty(PROP_APP_PATH_ON_SERVER);
	}

	public static String getDatabase() {
		return getInstance().getProperty(PROP_DATABASE);
	}

}
