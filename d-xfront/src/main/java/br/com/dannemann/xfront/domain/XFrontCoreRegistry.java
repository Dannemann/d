package br.com.dannemann.xfront.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.dannemann.core.domain.CoreRegistry;
import br.com.dannemann.core.domain.CoreRegistryUpdateException;
import br.com.dannemann.core.util.DUtilReflection;
import br.com.dannemann.persistence.domain.xfront.resolver.XFrontMetadataResolver;

public final class XFrontCoreRegistry {

	public static final String PROP_APP_PATH_ON_SERVER = DProperties.PROP_APP_PATH_ON_SERVER;
	public static final String PROP_ENTITY_CLASSES = "entity.classes";
	public static final String PROP_ENTITY_DESCRIPTORS = "entity.descriptors";

	public static void addApplication(final String appId) {
		CoreRegistry.addApplication(appId);
	}

	public static void setAppPathOnServer(final String appId, final String appPathOnServer) throws CoreRegistryUpdateException {
		CoreRegistry.addApplicationProperty(appId, DProperties.PROP_APP_PATH_ON_SERVER, appPathOnServer);
	}

	public static void loadEntities(final String appId, final String[] entityPackages) throws CoreRegistryUpdateException {
		CoreRegistry.addApplicationProperty(appId, PROP_ENTITY_CLASSES, DUtilReflection.loadClassesFromPkgs(entityPackages));
	}

	/**
	 * <p><b>Important:</b> This method must be executed after {@link #loadEntities(String, String[]) loadEntities(String, String[])}. 
	 * I've created it to keep {@link br.com.dannemann.persistence.domain.xfront.resolver.XFrontMetadataResolver XFrontMetadataResolver} related processing in a separate method.</p>
	 */
	public static void resolveEntitiesMetadata(final String appId, final XFrontMetadataResolver xFrontMetadataResolver) throws CoreRegistryUpdateException {
		final HashMap<String, ArrayList<Object>> entityDescriptors = new HashMap<>();

		for (final Class<?> entityBeanClass : (List<Class<?>>) CoreRegistry.getApplicationProperty(appId, PROP_ENTITY_CLASSES))
			entityDescriptors.put(entityBeanClass.getName(), xFrontMetadataResolver.resolve(entityBeanClass));

		xFrontMetadataResolver.afterResolve(entityDescriptors);

		CoreRegistry.addApplicationProperty(appId, PROP_ENTITY_DESCRIPTORS, entityDescriptors);
	}

}
